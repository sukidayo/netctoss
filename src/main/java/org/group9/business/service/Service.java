package org.group9.business.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.group9.pojo.Business;
import org.group9.util.Page;

public interface Service {
	List<Business> selectBusiness(Business business,Page page);
	Integer selectCount();
	Business selectBusinessById(int id);
	Integer updateBusiness(Business business);
	Integer deleteBusiness(int id);
	Integer selectId(String id);//查询是否有该身份证，并查出账户账号
	Integer addBusiness(Business business);
	Integer pauseState(int id);
	Integer startState(int id);

}
