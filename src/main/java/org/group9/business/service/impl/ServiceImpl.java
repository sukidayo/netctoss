package org.group9.business.service.impl;

import java.util.List;

import org.group9.accounting.mapper.AccountsMapper;
import org.group9.business.mapper.BussinessMapper;
import org.group9.business.service.Service;
import org.group9.pojo.Business;
import org.group9.util.Page;
import org.springframework.beans.factory.annotation.Autowired;





@org.springframework.stereotype.Service(value="businessService")
public class ServiceImpl implements Service {
	@Autowired
	BussinessMapper mapper;
	@Autowired
	AccountsMapper accountMapper;
	@Override
	public List<Business> selectBusiness(Business business, Page page) {
		// TODO Auto-generated method stub
		page.setStartCount();
		System.out.println(page.getCurPage());
		return mapper.selectBusiness(business, page);
	}
	@Override
	public Integer selectCount() {
		// TODO Auto-generated method stub
		return mapper.selectCount();
	}
	@Override
	public Business selectBusinessById(int id) {
		// TODO Auto-generated method stub
		return mapper.selectBusinessById(id);
	}
	@Override
	public Integer updateBusiness(Business business) {
		// TODO Auto-generated method stub
		return mapper.updateBusiness(business);
	}
	@Override
	public Integer deleteBusiness(int id) {
		// TODO Auto-generated method stub
		return mapper.deleteBusiness(id);
	}


	@Override
	public Integer selectId(String id) {
		// TODO Auto-generated method stub
		return mapper.selectId(id);
	}
	@Override
	public Integer addBusiness(Business business) {
		// TODO Auto-generated method stub
		return mapper.addBusiness(business);
	}
	@Override
	public Integer pauseState(int id) {
		// TODO Auto-generated method stub
		return mapper.pauseState(id);
	}
	@Override
	public Integer startState(int id) {
		// TODO Auto-generated method stub
		String state = accountMapper.selectStateById(id);
		System.out.println(state);
		if("暂停".equals(state))
			return 0;
		return mapper.startState(id);
	}


	
}
