package org.group9.business.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.group9.pojo.Business;
import org.group9.util.Page;

public interface BussinessMapper {
	List<Business> selectBusiness(@Param("business")Business business,@Param("page")Page page);
	Integer selectCount();
	Business selectBusinessById(int id);
	Integer updateBusiness(Business business);
	Integer deleteBusiness(int id);
	Integer selectId(String id);
	Integer addBusiness(Business business);
	Integer pauseState(int id);
	Integer startState(int id);

}
