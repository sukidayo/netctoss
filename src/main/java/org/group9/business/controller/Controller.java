package org.group9.business.controller;

import javax.servlet.http.HttpServletRequest;

import org.group9.business.service.Service;
import org.group9.pojo.Business;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@org.springframework.stereotype.Controller(value="business")
@RequestMapping("/business")
public class Controller {
	@Autowired
	Service service;
	@RequestMapping("/modi.do")
	public ModelAndView modi(Business business) {
		int i = service.updateBusiness(business);
		ModelAndView m = new ModelAndView("service_modi");
		if(i>0)
			m.addObject("message", "修改成功");
		else
			m.addObject("message", "修改失败");
		return m;
	}
	@RequestMapping("/delete.do")
	public String delete(@RequestParam int id,HttpServletRequest request) {
		int i = service.deleteBusiness(id);
		if(i>0)
			request.getSession().setAttribute("deleteBusinessMessage", "删除成功");
		else
			request.setAttribute("deleteBusinessMessage", "删除失败");
		return "redirect:http://localhost:8080/netctoss/admin/showView.do?click=6";
	}
	@RequestMapping(value="/selectid.do",produces="text/html;charset=utf-8")
	@ResponseBody
	public String selectId(HttpServletRequest request) {
		
		String data = null;
		String id = request.getParameter("id");
		System.out.println(id);
		if(id!=null) {
			Integer account = service.selectId(id);
			System.out.println(account);
			if(account!=null) {
				data=account+"";
			}
			else {
				data="未查询到此身份证";
			}
		}
		return data;
	}
	@RequestMapping("/addBusiness.do")
	public String addBusiness(Business business,HttpServletRequest request) {
		System.out.println(business.getPostageName());
		Integer i = service.addBusiness(business);
		if(i>0) {
			request.setAttribute("message", "添加成功");
			return "service_add";
		}
		else {
			request.setAttribute("message", "添加失败");
			return "service_add";
		}
	}
	@RequestMapping("/state.do")
	public String changeState(HttpServletRequest request,@RequestParam int state,@RequestParam int id) {
		//String state = request.getParameter("state");
		System.out.println(123);
		if(state==1) {
			int i = service.pauseState(id);
		}
		else {
			int i = service.startState(id);
			if(i==0) {
				request.getSession().setAttribute("changeMessage", "该账户已经暂停服务，无法开启");
				return "redirect:http://localhost:8080/netctoss/admin/showView.do?click=6";
			}
		}
		//return "redirect:http://localhost:8080/netctoss/admin/showView.do?click=6";

		return "redirect:http://localhost:8080/netctoss/admin/showView.do?click=6";
	}
}
