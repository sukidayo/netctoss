package org.group9.admin.service;

import java.util.List;

import org.group9.pojo.Admin;

public interface AdminService {
	//用户登录
	public Admin login(Admin admin);
	
	
	
	
	
	
	//查询所有管理员信息
	public List<Admin> selectAll();
	
	//查询当前管理员信息
	public Admin selectCurrentAdmin(Admin admin);
	
	//修改管理员信息
	public int updateAdmin(Admin admin);
	
	//修改密码
	public int updatePassword(Admin admin);
	
	//重置密码
	public int resetPassword(Admin admin);
	
	//添加管理员
	public int insertAdmin(Admin admin);
	
	//删除管理员	
	public int deleteAdmin(Admin admin);
	
	//根据管理员账号查询id
	public Admin selectIdByAccount(Admin admin);
	
	//添加管理员的权限
	public int insertPrivileage(int aid,int pid);
	
	//删除管理员的权限
	public int deletePrivileage(int aid);
}
