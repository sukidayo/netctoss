package org.group9.admin.service;

import java.util.List;

import org.group9.pojo.Privileage;

public interface PrivileageService {
	// 模糊查询权限名
	public List<String> selectPname(String privil);
	//根据aid查询权限名
	public String selectPnameByPid(int pid);
}
