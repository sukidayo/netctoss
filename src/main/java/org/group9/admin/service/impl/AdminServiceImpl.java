package org.group9.admin.service.impl;

import java.util.List;

import org.group9.admin.mapper.AdminMapper;
import org.group9.admin.service.AdminService;
import org.group9.pojo.Admin;
import org.group9.pojo.Privileage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
	@Autowired
	AdminMapper adminMapper;

	@Override
	//用户登录
	public Admin login(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.login(admin);
	}
	
	@Override
	public List<Admin> selectAll() {
		return adminMapper.selectAll();
	}

	@Override
	//修改密码
	public int updatePassword(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.updatePassword(admin);
	}

	@Override
	public int insertAdmin(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.insertAdmin(admin);
	}

	@Override
	public int deleteAdmin(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.deleteAdmin(admin);
	}

	@Override
	public int resetPassword(Admin admin) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateAdmin(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.updateAdmin(admin);
	}
	
	@Override
	public Admin selectIdByAccount(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.selectIdByAccount(admin);
	}

	@Override
	public int insertPrivileage(int aid,int pid) {
		// TODO Auto-generated method stub
		return adminMapper.insertPrivileage(aid,pid);
	}

	@Override
	public int deletePrivileage(int aid) {
		// TODO Auto-generated method stub
		return adminMapper.deletePrivileage(aid);
	}

	@Override
	public Admin selectCurrentAdmin(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.selectCurrentAdmin(admin);
	}
}
