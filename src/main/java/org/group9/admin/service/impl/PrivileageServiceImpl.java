package org.group9.admin.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.group9.admin.mapper.PrivileageMapper;
import org.group9.admin.service.PrivileageService;
import org.group9.pojo.Privileage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrivileageServiceImpl implements PrivileageService {
	@Resource
	PrivileageMapper privileageMapper;

	@Override
	public List<String> selectPname(String privil) {
		return privileageMapper.selectPname(privil);
	}

	@Override
	public String selectPnameByPid(int pid) {
		// TODO Auto-generated method stub
		return privileageMapper.selectPnameByPid(pid);
	}
}
