package org.group9.admin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import org.apache.ibatis.session.SqlSession;
import org.group9.admin.service.AdminService;
import org.group9.admin.service.PrivileageService;
import org.group9.pojo.Admin;
import org.group9.pojo.Business;
import org.group9.pojo.Postage;
import org.group9.pojo.Privileage;
import org.group9.tariff.service.Service;
import org.group9.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	Page page;
	@Autowired
	org.group9.business.service.Service businessService;
	@Autowired
	AdminService adminService;
	@Autowired
	PrivileageService privileageService;
	@Autowired
	Service tariffService;
	
	@RequestMapping("/showlogin.do")
	public String showLogin() {
		return "login";
	}
	
	@RequestMapping("/login.do") 
	public String login(Admin admin,HttpServletRequest request) {		
		Admin adm = adminService.login(admin);
		if(adm==null){
			request.setAttribute("Admin",admin);
			request.setAttribute("errorMsg", "用户名或密码错误！");
			return "login";
		}else{
			HttpSession session=request.getSession();
			session.setAttribute("currentAdmin", adm);
			return "index";			
		}		
	}
	
	@RequestMapping("/showView.do")
	public String showView(@RequestParam int click,HttpServletRequest request,Business business) {
		String s = null;
		boolean b = false;
		HttpSession session=request.getSession();
		Admin adm =(Admin)session.getAttribute("currentAdmin");
		Admin adm1 = adminService.selectIdByAccount(adm);
		List<Privileage> lp = adm1.getPrivil();
		List<Integer> li = new ArrayList<Integer>();
		List<String> ls = new ArrayList<String>();
		for(Privileage privil:lp) {
			li.add(privil.getPid());
			String pname = privileageService.selectPnameByPid(privil.getPid());			
			ls.add(pname);
		}			
		session.setAttribute("currentPrivilName", ls);
		switch (click) {
			case 1:
				s = "index";	
				break;
			case 2:
				b = li.contains(1)|| li.contains(2);
				if(b) {
					s = "role_list";
				}else {
					s = "nopower";
				}			
				break;
			case 3:
				b = li.contains(2) ;
				if(b) {
					s = "redirect:selectAll.do";
				}else {
					s = "nopower";
				}			
				break;
			case 4:
				b = li.contains(2) || li.contains(3);
				if(b) {
					int totalCount = tariffService.selectCount();
					String curPage = request.getParameter("curPage");
					page.setTotalCount(totalCount);
					page.setTotalPage(totalCount);
					if(curPage==null) {
						page.setCurPage(1);
						List<Postage> postageList = tariffService.selectPostage(page);
						System.out.println(postageList);
						request.setAttribute("page", page);
						request.setAttribute("tariffList", postageList);
					}
					else {
						System.out.println(curPage);
						int cur= Integer.parseInt(curPage);
						page.setCurPage(cur);
						List<Postage> postageList = tariffService.selectPostage(page);
						request.setAttribute("page", page);
						request.setAttribute("tariffList", postageList);
					}
					s = "fee_list";
				}else {
					s = "nopower";
				}	
				break;
			case 5:
				b = li.contains(2) || li.contains(4);
				if(b) {
					s = "forward:account_list.do";
				}else {
					s = "nopower";
				}	
				break;
			case 6:
				b = li.contains(2) || li.contains(5);
				if(b) {
					int totalCount = businessService.selectCount();
					String curPage = request.getParameter("curPage");
					page.setTotalCount(totalCount);
					page.setTotalPage(totalCount);
					if(curPage==null) {
						page.setCurPage(1);
						List<Business> businessList = businessService.selectBusiness(business, page);
						System.out.println(businessList);
						request.setAttribute("page", page);
						request.setAttribute("businessList", businessList);
					}
					else {
						System.out.println(curPage);
						int cur= Integer.parseInt(curPage);
						System.out.println(cur);
						page.setCurPage(cur);
						List<Business> businessList = businessService.selectBusiness(business, page);
						request.setAttribute("page", page);
						request.setAttribute("tariffList", businessList);
					}
					s = "service_list";
				}else {
					s = "nopower";
				}	
				break;
			case 7:
				b = li.contains(2) || li.contains(6);
				if(b) {
					s = "forward:bill_list.do";
				}else {
					s = "nopower";
				}	
				break;
			case 8:
				b = li.contains(2) || li.contains(7);
				if(b) {
					s = "report_list";
				}else {
					s = "nopower";
				}	
				break;
			case 9:
				Admin ad =(Admin)session.getAttribute("currentAdmin");
				Admin admin = adminService.selectCurrentAdmin(ad);
				session.setAttribute("currentAdminDetails", admin);
				s = "user_info";	
				break;
			case 10:
				s = "user_modi_pwd";
				break;
		}
		return s;
	}
	
	@RequestMapping("/account.do")
	public String showAccountView(@RequestParam int click) {
		String s = null;
		if(click == 1) {
			s = "account_add";
		}else if(click == 2){
			s = "account_detail";
		}else if(click == 3){
			s = "account_modi";
		}
		return s;
	}
	
	@RequestMapping("/adminView.do")
	public String showAdminView(@RequestParam int click,Admin admin) {
		String s = null;
		if(click == 1) {
			s = "admin_add";
		}else if(click == 2){
			s = "admin_modi";
		}
		return s;
	}
	
	@RequestMapping("/bill.do")
	public String showBillView(@RequestParam int click,HttpServletRequest request) {
		//List<Bill> bill = billservice.selectAllBill();*/
		String s = null;
		if(click == 1) {
			//request.getSession().setAttribute("bill", bill);
			s = "bill_item.do";
		}else if(click == 2){
			//request.getSession().setAttribute("bill", bill);
			s = "bill_service_detail.do";
		}
		return "forward:"+s;
	}
	
	@RequestMapping("/fee.do")
	public String showFeeView(@RequestParam int click,@RequestParam int id,HttpServletRequest request) {
		String s = null;
		if(click == 1) {
			s = "fee_add";
		}else if(click == 2){
			s = "fee_detail";
		}else if(click == 3){
			Postage p = tariffService.selectPostageByID(id);
			request.setAttribute("p", p);
			s = "fee_modi";
		}
		return s;
	}
	
	@RequestMapping("/role.do")
	public String showRoleView(@RequestParam int click) {
		String s = null;
		if(click == 1) {
			s = "role_add";
		}else if(click == 2){
			s = "role_modi";
		}
		return s;
	}
	
	@RequestMapping("/service.do")
	public String showServiceView(@RequestParam int click,@RequestParam int id,HttpServletRequest request) {
		String s = null;
		if(click == 1) {
			page.setCurPage(-1);
			List<Postage> p = tariffService.selectPostage(page);
			request.setAttribute("p", p);
			s = "service_add";
		}else if(click == 2){
			s = "service_detail";
		}else if(click == 3){ 
			Business b = businessService.selectBusinessById(id);
			page.setCurPage(-1);
			List<Postage> p = tariffService.selectPostage(page);
			request.setAttribute("b", b);
			request.setAttribute("p", p);
			s = "service_modi";
		}
		return s;
	}
	
	@RequestMapping("/selectAll.do")
	public String selectAll(HttpServletRequest request) {
		HttpSession session = request.getSession();
		List<Admin> ladm = adminService.selectAll();
		session.setAttribute("adminsInfor",ladm);
		return "admin_list";
	}
	
	@RequestMapping("/updateAdmin.do")
	public String updateAdmin(Admin admin,HttpServletRequest request) {
		String[] p = request.getParameterValues("pname");
		int i = adminService.updateAdmin(admin);
		int j = adminService.deletePrivileage(admin.getAid());
		int k = 0;
		for(String s: p) {				
			int pid = Integer.parseInt(s);
			k = adminService.insertPrivileage(admin.getAid(),pid);
		}
		if(i > 0 && j > 0 && k > 0) {
			request.getSession().setAttribute("flag", "修改成功");
		}else {
			request.getSession().setAttribute("flag", "修改失败");
		}
		return "forward:selectAll.do";
	}
	
	@RequestMapping("/updateCurrentAdmin.do")
	public String updateCurrentAdmin(Admin admin,HttpServletRequest request,Model m) {
		int i = adminService.updateAdmin(admin);
		if(i > 0) {
			request.getSession().setAttribute("flag", "修改成功");
		}else {
			request.getSession().setAttribute("flag", "修改失败");
		}
		m.addAttribute("currentAdminDetails", admin);
		return "user_info";
	}
	

	@RequestMapping("/updatePassword.do")
	public String updatePassword(Admin admin,HttpServletRequest request) {
		String newpsw = request.getParameter("password");
		String repsw = request.getParameter("repsw");
		if(newpsw != "" && newpsw.equals(repsw)) {
			int i = adminService.updatePassword(admin);
			if(i > 0) {
				request.getSession().setAttribute("flag", "修改成功");
				return "redirect:showlogin.do";
			}else {
				request.getSession().setAttribute("flag", "修改失败");
				return "user_modi_pwd";
			}			
		}else {
			request.getSession().setAttribute("flag", "密码为空或两次密码不一致！");
			return "user_modi_pwd";
		}		
	}
	
	/*@RequestMapping("/resetPassword.do")
	public String resetPassword(Admin admin,HttpServletRequest request) {
		String[] s = request.getParameterValues("details");
		int i = adminService.updatePassword(admin);
		if(i > 0) {
			request.setAttribute("aflag", "修改成功");
			return "redirect:showlogin.do";
		}else {
			request.setAttribute("aflag", "修改失败");
			return "forward:selectAll.do";
		}							
	}*/
	
	@RequestMapping("/insertAdmin.do")
	//添加信息最好用ajax验证表单
	public String insertAdmin(Admin admin,HttpServletRequest request) {
		String account = admin.getAccount();
		String newpsw = admin.getPassword();
		String repsw = request.getParameter("repsw");
		String[] p = request.getParameterValues("pname");
		if(account != "" && newpsw != "" && repsw != "" && newpsw.equals(repsw)) {
			int i = adminService.insertAdmin(admin);                   //插入管理员基本信息
			int aid = adminService.selectIdByAccount(admin).getAid();  //根据插入的管理员账号查询管理员的id
			
			int j = 0;
			for(String s: p) {				
				int pid = Integer.parseInt(s);
				j = adminService.insertPrivileage(aid,pid);
			}
			if(i > 0 && j > 0) {
				request.setAttribute("iflag", "添加成功");
				return "forward:selectAll.do";
			}else {
				request.setAttribute("iflag", "添加失败");
				return "admin_add";
			}		
		}else {
			if(account == "") {
				request.setAttribute("msg", "账号不能为空！");
			}
			if(newpsw == "") {
				request.setAttribute("msg1", "密码不能为空！");
			}
			if(repsw == "") {
				request.setAttribute("msg2", "请再次填写密码！");
			}else if(!newpsw.equals(repsw)) {
				request.setAttribute("msg2", "两次输入密码不一致！");
			}
			return "admin_add";
		} 		
	}
	
	@RequestMapping("/deleteAdmin.do")
	public String deleteAdmin(Admin admin,HttpServletRequest request) {
		int i = adminService.deleteAdmin(admin);
		int j = adminService.deletePrivileage(admin.getAid());
		if(i > 0 && j > 0) {
			request.setAttribute("aflag", "删除成功");
		}else {
			request.setAttribute("aflag", "删除失败");
		}
		return "forward:selectAll.do";
	}
	
	@RequestMapping("/selectAdminByCondition.do")
	public String selectAdminByCondition(HttpServletRequest request,Model m) {
		String s = request.getParameter("privil");
		List<String> strList = privileageService.selectPname(s);
		List<Admin> la = (List<Admin>)request.getSession().getAttribute("adminsInfor");
		List<Admin> la2 = new ArrayList<Admin>();
		for(Admin adm:la) {
			List<Privileage> privil = adm.getPrivil();
			out:
			for(Privileage pri : privil) {
				for(String str : strList) {
					if(str.equals(pri.getPname())) {
						la2.add(adm);
						break out;
					}
				}
			}
		}
		m.addAttribute("adminsInfor", la2);
		return "admin_list";
	}
}

