package org.group9.admin.mapper;


import java.util.List;

import org.group9.pojo.Privileage;

public interface PrivileageMapper {
	/**
	 * 根据aid查询权限名
	 */
	Privileage selectPnames(int aid);
	/**
	 * 模糊查询权限名
	 */
	List<String> selectPname(String privil);
	/**
	 * 根据aid查询权限id
	 */
	Privileage selectPids(int aid);
	/**
	 * 根据pid查询权限名
	 */
	String selectPnameByPid(int pid);
}
