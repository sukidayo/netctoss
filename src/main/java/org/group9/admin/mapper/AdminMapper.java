package org.group9.admin.mapper;

import java.util.List;

import org.group9.pojo.Admin;

public interface AdminMapper {
	/**
	 * 登录
	 * @return
	 */
	Admin login(Admin admin);
	/**
	 * 查询当前管理员信息
	 */
	Admin selectCurrentAdmin(Admin admin);
	/**
	 * 查询所有管理员信息
	 * @return
	 */
	List<Admin> selectAll();
	/**
	 * 修改管理员信息
	 */
	int updateAdmin(Admin admin);
	/**
	 * 修改密码
	 */
	int updatePassword(Admin admin);
	/**
	 * 重置密码
	 */
	//int resetPassword(Admin admin);
	/**
	 * 添加管理员信息
	 */
	int insertAdmin(Admin admin);
	/**
	 * 删除管理员信息
	 */
	int deleteAdmin(Admin admin);
	/**
	 * 根据管理员账号查询id
	 */
	Admin selectIdByAccount(Admin admin);
	/**
	 * 添加管理员的权限
	 */
	int insertPrivileage(int aid,int pid);
	/**
	 * 删除管理员的权限
	 */
	int deletePrivileage(int aid);
	 
}
