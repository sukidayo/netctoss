package org.group9.util;

import org.springframework.stereotype.Component;

@Component
public class Page {
	private int curPage;
	private int totalPage;
	private int pageCount=6;
	private int totalCount;
	private int startCount;
	public int getCurPage() {
		return curPage;
	}
	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalCount) {
		this.totalPage = (totalCount%pageCount==0)?totalCount/pageCount:(totalCount/pageCount)+1;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getStartCount() {
		return startCount;
	}
	public void setStartCount() {
		this.startCount = (curPage-1)*pageCount;
	}
	@Override
	public String toString() {
		return "Page [curPage=" + curPage + ", totalPage=" + totalPage + ", pageCount=" + pageCount + ", totalCount="
				+ totalCount + ", startCount=" + startCount + "]";
	}

}
