package org.group9.tariff.controller;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.group9.pojo.Postage;
import org.group9.tariff.service.Service;
import org.group9.tariff.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.stereotype.Controller(value="tariffController")
@RequestMapping("/postage")
public class Controller {
	@Autowired
	Service service;
	@Autowired
	org.group9.business.service.Service bservice;
	
	@RequestMapping("/addPostage.do")
	public String addPostage(Postage postage,HttpServletRequest request) {
		System.out.println(postage.getPostageName());
		int i = service.addPostage(postage);
		if(i>0) {
			request.getSession().setAttribute("message", "添加成功");
			return "fee_add";
		}
		request.getSession().setAttribute("message", "添加失败");
		return "fee_add";
	}
	@RequestMapping("/updatePostage.do")
	public String updatePostage(Postage postage,HttpServletRequest request) {
		int i = service.updatePostage(postage);
		if(i>0) {

			request.getSession().setAttribute("modiMessage", "修改成功");
			return "fee_modi";
		}
		request.getSession().setAttribute("modiMessage", "修改失败");
		return "fee_modi";
	}
	@RequestMapping("/deletePostage.do")
	public String deletePostage(@RequestParam int id,HttpServletRequest request) {
		int i = service.deletePostage(id);
		if(i>0) {
			request.getSession().setAttribute("deleteMessage", "删除成功");
			return "redirect:http://localhost:8080/netctoss/admin/showView.do?click=4";
		}
		request.getSession().setAttribute("deleteMessage", "删除失败");
		return "fee_list";
	}
	
	@RequestMapping("/startPostage.do")
	public String startPostage(@RequestParam int id,@RequestParam int state,HttpServletRequest request) {
		int i=0;
		System.out.println(id);
		if(state==1) {
		i = service.startPostage(id);
		}
		else {
			i = service.pausePostage(id);
		}
		if(i>0) {
			return "redirect:http://localhost:8080/netctoss/admin/showView.do?click=4";
		}
		
		return "redirect:http://localhost:8080/netctoss/admin/showView.do?click=4";
	}
	
}
