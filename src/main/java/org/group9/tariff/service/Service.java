package org.group9.tariff.service;

import java.util.List;

import org.group9.pojo.Postage;
import org.group9.util.Page;
import org.springframework.web.bind.annotation.RequestParam;

public interface Service {
	Integer addPostage(Postage postage);
	Integer selectCount();
	List<Postage> selectPostage(Page page);
	Postage selectPostageByID(int id);
	Integer updatePostage(Postage postage);
	Integer deletePostage(int id);
	Integer startPostage(int id);
	Integer pausePostage(int id);

}
