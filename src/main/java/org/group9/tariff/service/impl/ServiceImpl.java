package org.group9.tariff.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.group9.pojo.Postage;
import org.group9.tariff.mapper.TariffMapper;
import org.group9.tariff.service.Service;
import org.group9.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
@org.springframework.stereotype.Service
public class ServiceImpl implements Service {
	@Autowired
	TariffMapper mapper;
	@Override
	public Integer addPostage(Postage postage) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date date = new Date();
		postage.setCreationTime(format.format(date));;
		// TODO Auto-generated method stub
		return mapper.addPostage(postage);
	}
	@Override
	public Integer selectCount() {
		// TODO Auto-generated method stub
		return mapper.selectCount();
	}
	@Override
	public List<Postage> selectPostage(Page page) {
		// TODO Auto-generated method stub
		page.setStartCount();
		return mapper.selectPostage(page);
	}
	@Override
	public Postage selectPostageByID(int id) {
		// TODO Auto-generated method stub
		return mapper.selectPostageByID(id);
	}
	@Override
	public Integer updatePostage(Postage postage) {
		// TODO Auto-generated method stub
		return mapper.updatePostage(postage);
	}
	@Override
	public Integer deletePostage(int id) {
		// TODO Auto-generated method stub
		return mapper.deletePostage(id);
	}
	@Override
	public Integer startPostage(int id) {
		// TODO Auto-generated method stub
		return mapper.startPostage(id);
	}

	@Override
	public Integer pausePostage(int id) {
		// TODO Auto-generated method stub
		return mapper.pausePostage(id);
	}

	
}
