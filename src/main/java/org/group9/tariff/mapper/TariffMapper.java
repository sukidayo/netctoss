package org.group9.tariff.mapper;

import java.util.List;

import org.group9.pojo.Postage;
import org.group9.util.Page;

public interface TariffMapper {
	Integer addPostage(Postage postage);
	List<Postage> selectPostage(Page page);
	Integer selectCount();
	Postage selectPostageByID(int id);
	Integer updatePostage(Postage postage);
	Integer deletePostage(int id);
	Postage selectPostageByName(String postageName);
	Integer startPostage(int id);

	Integer pausePostage(int id);

}

