package org.group9.bill.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.group9.bill.service.BillService;
import org.group9.pojo.Bill;
import org.group9.pojo.Detail;
import org.group9.pojo.Specification;
import org.group9.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;



@Controller
@RequestMapping("/admin")
public class BillController {
	
	@Autowired
	BillService billService;
	
	String accountNum = null;
	String IP = null;
	@RequestMapping("/bill_list.do")
	public ModelAndView selectAllBill(HttpServletRequest request,Bill bill,Page page) {
		ModelAndView mav = new ModelAndView();
		
		//获取条件查询的值
		String idNumber = request.getParameter("idNumber");				//获取身份证号
		String accountNumber = request.getParameter("accountNumber");	//获取账务账号
		String userName = request.getParameter("userName");				//获取姓名

		String year = request.getParameter("year");						//获取年份
		String month = request.getParameter("month");					//获取月份
		String paymentTime = year+"-"+month;
		

		
		//判断是否是查询,如果是则添加%,准备模糊查询
		if(userName != null) {
			bill.setIdNumber(idNumber+"%");
			bill.setAccountNumber("%"+accountNumber+"%");
			bill.setUserName("%"+userName+"%");
			/*try {
				bill.setPaymentTime(new SimpleDateFormat("yyyy-MM").parse(paymentTime));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
		
		
		IP=null;
		accountNum=null;
		//总记录数
		int totalCount = billService.selectCount("bill",accountNum,IP);
		String curPage = request.getParameter("curPage");		//获取当前页数
		page.setTotalCount(totalCount);		
		page.setTotalPage(totalCount);
		int[] curPageNum = new int[page.getTotalPage()];
		for(int i = 0 ; i < curPageNum.length ; i++) {
			curPageNum[i] = i;
		}

		
		if(curPage==null) {
			page.setCurPage(1);
			bill.setPage(page);
			List<Bill> billList = billService.selectAllBill(bill);
			mav.addObject("bill",billList);
			mav.addObject("page", page);
			mav.addObject("curPageNum",curPageNum);
			mav.setViewName("bill_list");
		}
		else {
			int cur= Integer.parseInt(curPage);
			page.setCurPage(cur);
			page.setStartCount();
			bill.setPage(page);
			List<Bill> billList = billService.selectAllBill(bill);
			mav.addObject("bill",billList);
			mav.addObject("page", page);
			mav.addObject("curPageNum",curPageNum);
			mav.setViewName("bill_list");
		}	
		return mav;		
	}
	
	
	
	@RequestMapping("/bill_item.do")
	public ModelAndView selectAllDetail(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		String billID = null;
		int billID1 = 0;
		if(billID == null)
			billID = request.getParameter("billID");
		
		Bill bill1 = new Bill();
		if(billID != null) {
			billID1 = Integer.parseInt(billID);
		}
		Page page = new Page();
		page.setCurPage(-1);
		page.setStartCount();
		bill1.setPage(page);
		Bill bill = billService.selectAllBill(bill1).get(billID1-1);
		
		IP = null;
		accountNum = bill.getAccountNumber();
		//总记录数
		int totalCount = billService.selectCount("detail",accountNum,IP);
		String curPage = request.getParameter("curPage");		//获取当前页数
		page.setTotalCount(totalCount);		
		page.setTotalPage(totalCount);
		int[] curPageNum = new int[page.getTotalPage()];
		for(int i = 0 ; i < curPageNum.length ; i++) {
			curPageNum[i] = i;
		}
		Detail detail = new Detail();
		
		String accountNumber = request.getParameter("accountNumber");
		detail.setAccountNumber(accountNumber);
		
		if(curPage==null) {
			page.setCurPage(1);
			page.setStartCount();
			detail.setPage(page);
			List<Detail> detailList = billService.selectAllDetail(detail);
			System.out.println(detailList);
			mav.addObject("detail",detailList);
			mav.addObject("bill",bill);
			mav.addObject("page", page);
			mav.addObject("curPageNum",curPageNum);
			mav.addObject("billID", billID1);
			mav.setViewName("bill_item");
		}
		else {
			int cur= Integer.parseInt(curPage);
			page.setCurPage(cur);
			page.setStartCount();
			detail.setPage(page);
			List<Detail> detailList = billService.selectAllDetail(detail);
			mav.addObject("detail",detailList);
			mav.addObject("bill",bill);
			mav.addObject("page", page);
			mav.addObject("curPageNum",curPageNum);
			mav.addObject("billID", billID1);
			mav.setViewName("bill_item");
		}	
		return mav;		
	}
	
	
	
	@RequestMapping("/bill_service_detail.do")
	public ModelAndView selectAllSpecification(HttpServletRequest request) {
		Detail detail1 = new Detail();
		String serverIP = request.getParameter("serverIP");
		String accountNumber = request.getParameter("accountNumber");
		detail1.setAccountNumber(accountNumber);
		
		int billID1 = 0;
		String billID = request.getParameter("billID");
		if(billID != null) {
			billID1 = Integer.parseInt(billID);
		}
		
		detail1.setServerIP(serverIP);
		Page page = new Page();
		page.setCurPage(-1);
		page.setStartCount();
		detail1.setPage(page);
		
		List<Detail> detailList = billService.selectAllDetail(detail1);
		
		Detail detail = new Detail();
		for (Detail detail2:detailList) {
			detail = detail2;
		}
		ModelAndView mav = new ModelAndView();
		Specification specification = new Specification();
		
		accountNum = null;
		IP = detail1.getServerIP();
		//总记录数
		int totalCount = billService.selectCount("specification",accountNum,IP);
		String curPage = request.getParameter("curPage");		//获取当前页数
		page.setTotalCount(totalCount);		
		page.setTotalPage(totalCount);
		int[] curPageNum = new int[page.getTotalPage()];
		for(int i = 0 ; i < curPageNum.length ; i++) {
			curPageNum[i] = i;
		}
		specification.setDetail(detail);
		if(curPage==null) {
			page.setCurPage(1);
			page.setStartCount();
			specification.setPage(page);
			
			List<Specification> specificationList = billService.selectAllSpecification(specification);
			mav.addObject("specificationList",specificationList);
			mav.addObject("detail", detail);
			mav.addObject("billID", billID1);
			mav.addObject("page", page);
			mav.addObject("curPageNum",curPageNum);
			mav.setViewName("bill_service_detail");
		}
		else {
			int cur= Integer.parseInt(curPage);
			page.setCurPage(cur);
			page.setStartCount();
			specification.setPage(page);
			List<Specification> specificationList = billService.selectAllSpecification(specification);
			mav.addObject("specificationList",specificationList);
			mav.addObject("detail", detail);
			mav.addObject("billID", billID1);
			mav.addObject("page", page);
			mav.addObject("curPageNum",curPageNum);
			mav.setViewName("bill_service_detail");
		}	
		return mav;
	}
}
