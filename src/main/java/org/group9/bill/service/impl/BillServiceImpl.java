package org.group9.bill.service.impl;

import java.util.List;

import org.group9.bill.mapper.BillMapper;
import org.group9.bill.service.BillService;
import org.group9.pojo.Bill;
import org.group9.pojo.Detail;
import org.group9.pojo.Specification;
import org.group9.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BillServiceImpl implements BillService {
	@Autowired
	BillMapper billMapper;
	
	@Override
	public List<Bill> selectAllBill(Bill bill) {
		// TODO Auto-generated method stub
		return billMapper.selectAllBill(bill);
	}

	@Override
	public List<Detail> selectAllDetail(Detail detail) {
		// TODO Auto-generated method stub
		return billMapper.selectAllDetail(detail);
	}
	
	@Override
	public List<Specification> selectAllSpecification(Specification specification) {
		// TODO Auto-generated method stub
		return billMapper.selectAllSpecification(specification);
	}

	@Override
	public Integer selectCount(String table,String accountNum,String IP) {
		// TODO Auto-generated method stub
		return billMapper.selectCount(table,accountNum,IP);
	}
}
