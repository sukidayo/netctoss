package org.group9.bill.service;

import java.util.List;

import org.group9.pojo.Bill;
import org.group9.pojo.Detail;
import org.group9.pojo.Specification;
import org.group9.util.Page;

public interface BillService {
	/**
	 * 查询所有账单
	 * @return
	 */
	public List<Bill> selectAllBill(Bill bill);
	
	/**
	 * 查询所有明细
	 * @return
	 */
	public List<Detail> selectAllDetail(Detail detail);
	
	/**
	 * 查询所有详单
	 * @return
	 */
	public List<Specification> selectAllSpecification(Specification specification);
	/**
	 * 查询总记录数
	 * @return
	 */
	public Integer selectCount(String table,String accountNum,String IP);
	
}
