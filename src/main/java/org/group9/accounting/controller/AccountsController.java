package org.group9.accounting.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.group9.accounting.service.AccountsService;
import org.group9.business.service.Service;
import org.group9.pojo.Accounts;
import org.group9.pojo.Business;
import org.group9.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin")
public class AccountsController {
	
	@Autowired
	AccountsService accountsservice;
	/*@Autowired
	Service service;*/
	private Integer accountID;
	//查询所有账务账单信息
	@RequestMapping("/account_list.do")
	public ModelAndView selectAll(Accounts accounts,Page page,HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();

		int totalCount = accountsservice.selectCount();
		String curPage = request.getParameter("curPage");		//获取当前页数
	
		page.setTotalCount(totalCount);		
		page.setTotalPage(totalCount);
		int[] curPageNum = new int[page.getTotalPage()];
		for(int i = 0 ; i < curPageNum.length ; i++) {
			curPageNum[i] = i;
		}
		
		if(curPage==null) {
			page.setCurPage(1);
			//System.out.println(curPage);
			accounts.setPage(page);
			List<Accounts> account = accountsservice.selectAll(accounts);
			mav.addObject("account",account);
			mav.setViewName("account_list");
			mav.addObject("page", page);
			mav.addObject("curPageNum",curPageNum);

		}
		else {
			int cur= Integer.parseInt(curPage);
			page.setCurPage(cur);
			page.setStartCount();
			accounts.setPage(page);
			List<Accounts> account = accountsservice.selectAll(accounts);
			mav.addObject("account",account);
			mav.setViewName("account_list");
			mav.addObject("page", page);
			mav.addObject("curPageNum",curPageNum);
		}	
		return mav;
		/*ModelAndView mav = new ModelAndView();
		List<Accounts> account = accountsservice.selectAll(accounts);
		mav.addObject("account",account);
		mav.setViewName("account_list");
		return mav;	*/
	}
	//添加账务账单
	@RequestMapping("/account_add.do")
	public String addAccounts(Accounts accounts,Business business,HttpServletRequest request) {
		
		int i = accountsservice.addAccounts(accounts);
		String idNumber= accounts.getIdNumber();
		Integer a =accountsservice.selectID(idNumber);
		business.setAccountID(a);
		business.setState("开通");
		business.setServerIP("192.168.1");
		business.setPostageName("亲子套餐");
		Integer b = accountsservice.addBusiness(business);
		if(b>0) {
			request.getSession().setAttribute("message", "添加成功");
			return "account_add";
		}
		request.getSession().setAttribute("message", "添加失敗");
		return "account_add";		
	}
	
	//修改账务账号
	@RequestMapping("/account_modi.do")
	public String updateUser(Accounts accounts,HttpServletRequest request) {
		int i = accountsservice.updateAccounts(accounts);
		if(i > 0) {
			request.getSession().setAttribute("message","修改成功");
			return "account_modi";
		}
		request.getSession().setAttribute("message","修改失败");
		return "account_modi ";
	}
	@RequestMapping("/deleteAccount.do")
	public String deleteAccount(@RequestParam int accountID,HttpServletRequest request) {
		//System.out.println(accountID);
		int i = accountsservice.deleteAccounts(accountID);
		System.out.println(i);
		if(i > 0) {
			request.getSession().setAttribute("deleteMessage", "删除成功");
			return "redirect:http://localhost:8080/netctoss/admin/showView.do?click=5";
		}
		request.getSession().setAttribute("deleteMessage", "删除失败");
		return "account_list";
	}
	@RequestMapping("/updateState.do")
	public String updateState(@RequestParam String id,Accounts accounts,HttpServletRequest request) {
		String state = id.substring(0, 2);
		int accountID = Integer.parseInt(id.substring(2,5));
		accounts.setState(state);
		accounts.setAccountID(accountID);
		int i = accountsservice.updateState(accounts);
		if(i > 0) {
			request.getSession().setAttribute("updateStateMessage", "修改状态成功");
			return "redirect:http://localhost:8080/netctoss/admin/showView.do?click=5";
		}
		request.getSession().setAttribute("updateStateMessage", "修改状态失败");
		return "redirect:http://localhost:8080/netctoss/admin/showView.do?click=5";
	}

}
