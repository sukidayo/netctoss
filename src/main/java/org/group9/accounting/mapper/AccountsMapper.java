package org.group9.accounting.mapper;

import java.util.List;

import org.group9.pojo.Accounts;
import org.group9.pojo.Business;
import org.group9.util.Page;

public interface AccountsMapper {
	List<Accounts> selectAll(Accounts accounts);//查询所有账务
	Integer addAccounts(Accounts accounts);//添加账务
	Integer updateAccounts(Accounts accounts);//修改账务信息
	Integer deleteAccounts(int accountID);//删除账务信息

	Integer startState(Accounts accounts);//修改状态信息
	Integer pauseState(Accounts accounts);
	Integer selectID(String idNumber);
	Integer addBusiness(Business business);
	Integer selectCount();//查询数据总数

	//Integer selectCount();//查询数据条数
	String selectStateById(int id);

}
