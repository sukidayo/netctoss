package org.group9.accounting.service.impl;

import java.util.List;

import org.apache.log4j.net.SyslogAppender;
import org.group9.accounting.mapper.AccountsMapper;
import org.group9.accounting.service.AccountsService;
import org.group9.business.mapper.BussinessMapper;
import org.group9.pojo.Accounts;
import org.group9.pojo.Business;
import org.group9.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class AccountsServiceImpl implements AccountsService {
	@Autowired
	AccountsMapper accountsmapper;
	/*@Autowired
	BussinessMapper bussinessmapper;*/
	@Override
	public List<Accounts> selectAll(Accounts accounts) {
		// TODO Auto-generated method stub
		return accountsmapper.selectAll(accounts);
	}
	@Override
	public Integer addAccounts(Accounts accounts) {
		// TODO Auto-generated method stub
		/*int i = 0 ;
		 i += accountsmapper.addAccounts(accounts);
		 
		String idNumber= accounts.getIdNumber();
		System.out.println(idNumber);
		Integer accountID = accounts.getAccountID();
		System.out.println(accountID);*/
		//System.out.println(accountsmapper.selectID(idNumber));
		//System.out.println(accountID);
		/*String state = accounts.getIdNumber();
		Business business = new Business();
		business.setAccountID(accountID);
		business.setState(state);
		i += accountsmapper.addBusiness(business);
		return accountsmapper.addAccounts(accounts);*/
		return accountsmapper.addAccounts(accounts);
				
	}
	@Override
	public Integer updateAccounts(Accounts accounts) {
		// TODO Auto-generated method stub
		return accountsmapper.updateAccounts(accounts);
	}
	@Override
	public Integer deleteAccounts(int accountID) {
		// TODO Auto-generated method stub
		return accountsmapper.deleteAccounts(accountID);
	}
	@Override
	public Integer updateState(Accounts accounts) {
		// TODO Auto-generated method stub
		String state = accounts.getState();
		Integer accountID = accounts.getAccountID();
		if(state.equals("开通")) {
			return accountsmapper.pauseState(accounts);
		}else{
			return accountsmapper.startState(accounts);
			
		}		
	}
	@Override
	public Integer selectCount() {
		// TODO Auto-generated method stub
		return accountsmapper.selectCount();
	}
	@Override
	public Integer selectID(String idNumber) {
		// TODO Auto-generated method stub
		return accountsmapper.selectID(idNumber);
	}
	@Override
	public Integer addBusiness(Business business) {
		// TODO Auto-generated method stub
		return accountsmapper.addBusiness(business);
	}
	
}
