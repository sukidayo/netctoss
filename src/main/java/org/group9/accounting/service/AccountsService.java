package org.group9.accounting.service;

import java.util.List;

import org.group9.pojo.Accounts;
import org.group9.pojo.Business;
import org.group9.pojo.User;
import org.group9.util.Page;

public interface AccountsService {
	List<Accounts> selectAll(Accounts accounts);//查询所有账务
	Integer addAccounts(Accounts accounts);//添加账务
	Integer updateAccounts(Accounts accounts);//修改账务信息
	Integer deleteAccounts(int accountID);//删除账务信息
	Integer updateState(Accounts accounts);//修改状态信息	
	//String findAccountByID(String idNumber);
	//Integer addBusiness(Business business);
	Integer selectCount();//查询数据总数
	Integer selectID(String idNumber);
	Integer addBusiness(Business business);

}
