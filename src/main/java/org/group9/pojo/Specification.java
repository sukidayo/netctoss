package org.group9.pojo;

import org.group9.util.Page;

public class Specification {
	private String userLoginIp;
	private String userLoginTime;
	private String userExitTime;
	private String time;
	private double cost;
	private String postageName;
	private String bf1;
	private int bf2;
	private String flag = "specification";
	
	private Page page;
	private Detail detail;
	
	public String getUserLoginIp() {
		return userLoginIp;
	}
	public void setUserLoginIp(String userLoginIp) {
		this.userLoginIp = userLoginIp;
	}
	public String getUserLoginTime() {
		return userLoginTime;
	}
	public void setUserLoginTime(String userLoginTime) {
		this.userLoginTime = userLoginTime;
	}
	public String getUserExitTime() {
		return userExitTime;
	}
	public void setUserExitTime(String userExitTime) {
		this.userExitTime = userExitTime;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public String getPostageName() {
		return postageName;
	}
	public void setPostageName(String postageName) {
		this.postageName = postageName;
	}
	public String getBf1() {
		return bf1;
	}
	public void setBf1(String bf1) {
		this.bf1 = bf1;
	}
	public int getBf2() {
		return bf2;
	}
	public void setBf2(int bf2) {
		this.bf2 = bf2;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	@Override
	public String toString() {
		return "Specification [userLoginIp=" + userLoginIp + ", userLoginTime=" + userLoginTime + ", userExitTime="
				+ userExitTime + ", time=" + time + ", cost=" + cost + ", postageName=" + postageName + ", page=" + page
				+ "]";
	}
	public Detail getDetail() {
		return detail;
	}
	public void setDetail(Detail detail) {
		this.detail = detail;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
}
