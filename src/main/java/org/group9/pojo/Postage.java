package org.group9.pojo;

import java.util.Date;

public class Postage {
	private int postageId;
	private String postageName;
	private String basicTime;
	private Double basicCost;
	private Double unitCost;
	private String creationTime;
	private String openTime;
	private String state;
	private String information;
	public int getPostageId() {
		return postageId;
	}
	public void setPostageId(int postageId) {
		this.postageId = postageId;
	}
	public String getPostageName() {
		return postageName;
	}
	public void setPostageName(String postageName) {
		this.postageName = postageName;
	}
	public String getBasicTime() {
		return basicTime;
	}
	public void setBasicTime(String basicTime) {
		this.basicTime = basicTime;
	}
	public Double getBasicCost() {
		return basicCost;
	}
	public void setBasicCost(Double basicCost) {
		this.basicCost = basicCost;
	}
	public Double getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	public String getOpenTime() {
		return openTime;
	}
	public void setOpenTime(String openTime) {
		this.openTime = openTime;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getInformation() {
		return information;
	}
	public void setInformation(String information) {
		this.information = information;
	}
	
}