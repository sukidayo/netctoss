package org.group9.pojo;

import org.group9.util.Page;

public class Detail {
	private int detailedID;
	private String osNumber;
	private String serverIP;
	private String accountNumber;
	private String lengthTime;
	private double basicCost;
	private String postageName;
	private String be1;
	private int be2;
	private String flag = "detail";
	private Page page;
	public int getDetailedID() {
		return detailedID;
	}
	public void setDetailedID(int detailedID) {
		this.detailedID = detailedID;
	}
	public String getOsNumber() {
		return osNumber;
	}
	public void setOsNumber(String osNumber) {
		this.osNumber = osNumber;
	}
	public String getServerIP() {
		return serverIP;
	}
	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getLengthTime() {
		return lengthTime;
	}
	public void setLengthTime(String lengthTime) {
		this.lengthTime = lengthTime;
	}
	public double getBasicCost() {
		return basicCost;
	}
	public void setBasicCost(double basicCost) {
		this.basicCost = basicCost;
	}
	public String getPostageName() {
		return postageName;
	}
	public void setPostageName(String postageName) {
		this.postageName = postageName;
	}
	public String getBe1() {
		return be1;
	}
	public void setBe1(String be1) {
		this.be1 = be1;
	}
	public int getBe2() {
		return be2;
	}
	public void setBe2(int be2) {
		this.be2 = be2;
	}
	
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	@Override
	public String toString() {
		return "Detail [detailedID=" + detailedID + ", osNumber=" + osNumber + ", serverIP=" + serverIP
				+ ", accountNumber=" + accountNumber + ", lengthTime=" + lengthTime + ", basicCost=" + basicCost
				+ ", postageName=" + postageName + ", be1=" + be1 + ", be2=" + be2 + ", page=" + page + "]";
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
}
