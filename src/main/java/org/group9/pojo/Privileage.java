package org.group9.pojo;

import java.util.List;

public class  Privileage{
	private Integer pid;
	private String pname;

	@Override
	public String toString() {
		return pname;
	}
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	
}
