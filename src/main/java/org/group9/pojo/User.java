package org.group9.pojo;
public class User {
	private Integer userID;
	private Integer accountID;
	private String customerName;
	private String idNumber;
	private String userAccount;
	private String userPassword;
	private String userPhone;
	private String reIdNumber;
	private String reBirthday;
	private String reEmail;
	private String reWork;
	private String reSex;
	private String reAddress;
	private String reCode;
	private String reQQ;
	
	public User() {
		super();
	}
	public User(String customerName,String userPassword,String userPhone,String reIdNumber,String reEmail,String reWork,
			String reSex,String reAdress,String reCode,String reQQ) {
		super();
		this.customerName = customerName;
		this.userPassword = userPassword;
		this.userPhone = userPhone;
		this.reIdNumber = reIdNumber;
		this.reEmail = reEmail;
		this.reWork = reWork;
		this.reSex = reSex;
		this.reAddress = reAddress;
		this.reCode = reCode;
		this.reQQ = reQQ;
	}
	
	public User(Integer userID, Integer accountID, String customerName, String idNumber, String userAccount,
			String userPassword, String userPhone, String reIdNumber, String reBirthday, String reEmail, String reWork,
			String reSex, String reAddress, String reCode, String reQQ) {
		super();
		this.userID = userID;
		this.accountID = accountID;
		this.customerName = customerName;
		this.idNumber = idNumber;
		this.userAccount = userAccount;
		this.userPassword = userPassword;
		this.userPhone = userPhone;
		this.reIdNumber = reIdNumber;
		this.reBirthday = reBirthday;
		this.reEmail = reEmail;
		this.reWork = reWork;
		this.reSex = reSex;
		this.reAddress = reAddress;
		this.reCode = reCode;
		this.reQQ = reQQ;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public Integer getAccountID() {
		return accountID;
	}

	public void setAccountID(Integer accountID) {
		this.accountID = accountID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getReIdNumber() {
		return reIdNumber;
	}

	public void setReIdNumber(String reIdNumber) {
		this.reIdNumber = reIdNumber;
	}

	public String getReBirthday() {
		return reBirthday;
	}

	public void setReBirthday(String reBirthday) {
		this.reBirthday = reBirthday;
	}

	public String getReEmail() {
		return reEmail;
	}

	public void setReEmail(String reEmail) {
		this.reEmail = reEmail;
	}

	public String getReWork() {
		return reWork;
	}

	public void setReWork(String reWork) {
		this.reWork = reWork;
	}

	public String getReSex() {
		return reSex;
	}

	public void setReSex(String reSex) {
		this.reSex = reSex;
	}

	public String getReAddress() {
		return reAddress;
	}

	public void setReAddress(String reAddress) {
		this.reAddress = reAddress;
	}

	public String getReCode() {
		return reCode;
	}

	public void setReCode(String reCode) {
		this.reCode = reCode;
	}

	public String getReQQ() {
		return reQQ;
	}

	public void setReQQ(String reQQ) {
		this.reQQ = reQQ;
	}
	 
	
}
