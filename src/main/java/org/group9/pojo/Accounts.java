package org.group9.pojo;

import org.group9.util.Page;

public class Accounts {
	private Integer accountID;        //账号ID
	private String userName;		  //用户姓名
	private String idNumber;	      //身份证号
	private String userAccount;       //用户账号
	private String userPassword; 	  //用户密码
	private String userPhone;         //用户电话
	private String state;			  //开通状态
	private String createTime;		  //开通时间
	private String lastLoginTime;     //上次登录时间
	private String accountNumber;
	private String reIdNumber;
	private String reBirthday;
	private String reEmail;
	private String reWork;
	private String reSex;
	private String reAddress;
	private String reCode;
	private String reQQ;
	
	private Page page;
	
	public Accounts() {
		super();
	}
	public Accounts(Integer accountID) {
		super();
		this.accountID = accountID;
	}
	public Accounts(Integer accountID, String userName, String idNumber, String userAccount,String userPassword, String userPhone,
			String state, String createTime, String lastLoginTime, String accountNumber, String reIdNumber,
			String reBirthday, String reEmail, String reWork, String reSex, String reAddress, String reCode,
			String reQQ) {
		super();
		this.accountID = accountID;
		this.userName = userName;
		this.idNumber = idNumber;
		this.userAccount = userAccount;
		this.userPassword = userPassword;
		this.userPhone = userPhone;
		this.state = state;
		this.createTime = createTime;
		this.lastLoginTime = lastLoginTime;
		this.accountNumber = accountNumber;
		this.reIdNumber = reIdNumber;
		this.reBirthday = reBirthday;
		this.reEmail = reEmail;
		this.reWork = reWork;
		this.reSex = reSex;
		this.reAddress = reAddress;
		this.reCode = reCode;
		this.reQQ = reQQ;
	}
	public Integer getAccountID() {
		return accountID;
	}
	public void setAccountID(Integer accountID) {
		this.accountID = accountID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public String getUserPassword(String userPassword) {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String openTime) {
		this.createTime = openTime;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getReIdNumber() {
		return reIdNumber;
	}
	public void setReIdNumber(String reIdNumber) {
		this.reIdNumber = reIdNumber;
	}
	public String getReBirthday() {
		return reBirthday;
	}
	public void setReBirthday(String reBirthday) {
		this.reBirthday = reBirthday;
	}
	public String getReEmail() {
		return reEmail;
	}
	public void setReEmail(String reEmail) {
		this.reEmail = reEmail;
	}
	public String getReWork() {
		return reWork;
	}
	public void setReWork(String reWork) {
		this.reWork = reWork;
	}
	public String getReSex() {
		return reSex;
	}
	public void setReSex(String reSex) {
		this.reSex = reSex;
	}
	public String getReAddress() {
		return reAddress;
	}
	public void setReAddress(String reAddress) {
		this.reAddress = reAddress;
	}
	public String getReCode() {
		return reCode;
	}
	public void setReCode(String reCode) {
		this.reCode = reCode;
	}
	public String getReQQ() {
		return reQQ;
	}
	public void setReQQ(String reQQ) {
		this.reQQ = reQQ;
	}
	
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	@Override
	public String toString() {
		return "Accounts [accountID=" + accountID + ", userName=" + userName + ", idNumber=" + idNumber
				+ ", userAccount=" + userAccount + ", userPassword=" + userPassword + ", userPhone=" + userPhone
				+ ", state=" + state + ", openTime=" + createTime + ", lastLoginTime=" + lastLoginTime
				+ ", accountNumber=" + accountNumber + ", reIdNumber=" + reIdNumber + ", reBirthday=" + reBirthday
				+ ", reEmail=" + reEmail + ", reWork=" + reWork + ", reSex=" + reSex + ", reAddress=" + reAddress
				+ ", reCode=" + reCode + ", reQQ=" + reQQ + "]";
	}
	
	
}
