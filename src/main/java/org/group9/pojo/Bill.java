package org.group9.pojo;

import java.util.Date;

import org.group9.util.Page;

public class Bill {
	private int billID;
	private String userName;
	private String idNumber;
	private String accountNumber;
	private double cost;
	private Date paymentTime;
	private String paymentMethod;
	private String paymentState;
	private String bf1;
	private int bf2;
	private Page page;
	public int getBillID() {
		return billID;
	}
	public void setBillID(int billID) {
		this.billID = billID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public Date getPaymentTime() {
		return paymentTime;
	}
	public void setPaymentTime(Date paymentTime) {
		this.paymentTime = paymentTime;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPaymentState() {
		return paymentState;
	}
	public void setPaymentState(String paymentState) {
		this.paymentState = paymentState;
	}
	public String getBf1() {
		return bf1;
	}
	public void setBf1(String bf1) {
		this.bf1 = bf1;
	}
	public int getBf2() {
		return bf2;
	}
	public void setBf2(int bf2) {
		this.bf2 = bf2;
	}
	@Override
	public String toString() {
		return "Bill [billID=" + billID + ", userName=" + userName + ", idNumber=" + idNumber + ", accountNumber="
				+ accountNumber + ", cost=" + cost + ", paymentTime=" + paymentTime + ", paymentMethod=" + paymentMethod
				+ ", paymentState=" + paymentState + "]";
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	
	
}
