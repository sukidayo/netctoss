package org.group9.pojo;

import java.util.List;

public class Admin {
	private Integer aid;
	private String aname;
	private String account;
	private String password;
	private String aphone;
	private String aemail;
	private String photo;
	private List<Privileage> privil;
	public Integer getAid() {
		return aid;
	}
	public void setAid(Integer aid) {
		this.aid = aid;
	}
	public String getAname() {
		return aname;
	}
	public void setAname(String aname) {
		this.aname = aname;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAphone() {
		return aphone;
	}
	public void setAphone(String aphone) {
		this.aphone = aphone;
	}
	public String getAemail() {
		return aemail;
	}
	public void setAemail(String aemail) {
		this.aemail = aemail;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public List<Privileage> getPrivil() {
		return privil;
	}
	public void setPrivil(List<Privileage> privil) {
		this.privil = privil;
	}
	@Override
	public String toString() {
		return "Admin [aid=" + aid + ", aname=" + aname + ", account=" + account + ", password=" + password
				+ ", aphone=" + aphone + ", aemail=" + aemail + ", privil=" + privil + "]";
	}
	
}
