<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global.css" />
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global_color.css" />
        <script src="${pageContext.request.contextPath}/js/jquery-1.7.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/json2.js"></script> 
        <script language="javascript" type="text/javascript">
            //排序按钮的点击事件
            function sort(btnObj) {
                if (btnObj.className == "sort_desc")
                    btnObj.className = "sort_asc";
                else
                    btnObj.className = "sort_desc";
            }
            function showResult() {
                showResultDiv(true);
                window.setTimeout("showResultDiv(false);", 3000);
            }
            function showResultDiv(flag) {
                var divResult = document.getElementById("operate_result_info");
                if (flag)
                    divResult.style.display = "block";
                else
                    divResult.style.display = "none";
            }

            //启用
            function startFee(id) {
            	var rows = id.parentNode.parentNode.rowIndex;
            	var table = document.getElementById("datalist");
            	var pid = table.rows[rows].cells[0].innerHTML;
            	 var state = id.value
                 if(state=="暂停"){
                	 window.confirm("确定要暂停此资费吗？");
                 	id.value="启用";
                 	location.href='${pageContext.request.contextPath}/postage/startPostage.do?id='+pid+'&state=2'
                 }
                 else{
                	 window.confirm("确定要启用此资费吗？资费启用后将不能修改和删除。");
                 	id.value="暂停";
                 	location.href='${pageContext.request.contextPath}/postage/startPostage.do?id='+pid+'&state=1'
                 }
                
            	
            	}
            //显示删除信息
            function showDelete() {
               
                document.getElementById("operate_result_info").style.display = "block";
            }
            //删除
            function deletePostage(id){
            	var r = window.confirm("确定要删除此资费吗？");
            	var rows = id.parentNode.parentNode.rowIndex;
            	var table = document.getElementById("datalist");
            	var pid = table.rows[rows].cells[0].innerHTML;
            	location.href='${pageContext.request.contextPath}/postage/deletePostage.do?id='+pid
            }
        </script>        
    </head>
    <body>
        <!--Logo区域开始-->
        <div id="header">
            <img src="../images/logo.png" alt="logo" class="left"/>
			<span>当前账号：<b>${currentAdmin.account}</b></span>
            <a href="${pageContext.request.contextPath }/admin/showlogin.do">[退出]</a>            
        </div>
        <!--Logo区域结束-->
        <!--导航区域开始-->
        <div id="navi">
	    	<ul id="menu">
		        <li><a href="../admin/showView.do?click=1" class="index_off"></a></li>
		        <li><a href="../admin/showView.do?click=2" class="role_off"></a></li>
		        <li><a href="../admin/showView.do?click=3" class="admin_off"></a></li>
		        <li><a href="../admin/showView.do?click=4" class="fee_on"></a></li>
		        <li><a href="../admin/showView.do?click=5" class="account_off"></a></li>
		        <li><a href="../admin/showView.do?click=6" class="service_off"></a></li>
		        <li><a href="../admin/showView.do?click=7" class="bill_off"></a></li>
		        <li><a href="../admin/showView.do?click=8" class="report_off"></a></li>
		        <li><a href="../admin/showView.do?click=9" class="information_off"></a></li>
		        <li><a href="../admin/showView.do?click=10" class="password_off"></a></li>
	    	</ul>
		</div>
        <!--导航区域结束-->
        <!--主要区域开始-->
        <div id="main">
            <form action="" method="">
                <!--排序-->
                <div class="search_add">
                    <div>
                        <input type="button" value="月租" class="sort_asc" onclick="sort(this);" />
                        <input type="button" value="基费" class="sort_asc" onclick="sort(this);" />
                        <input type="button" value="时长" class="sort_asc" onclick="sort(this);" />
                    </div>
                    <input type="button" value="增加" class="btn_add" onclick="location.href='${pageContext.request.contextPath }/admin/fee.do?click=1&id=0';" />
                </div> 
                <!--启用操作的操作提示-->
                <div id="operate_result_info" class="operate_success">
                    <img src="../images/close.png" onclick="this.parentNode.style.display='none';" />
                    ${deleteMessage}
                </div>    
                <!--数据区域：用表格展示数据-->     
                <div id="data">            
                    <table id="datalist">
                        <tr>
                            <th>资费ID</th>
                            <th class="width100">资费名称</th>
                            <th>基本时长</th>
                            <th>基本费用</th>
                            <th>单位费用</th>
                            <th>创建时间</th>
                            <th>开通时间</th>
                            <th class="width50">状态</th>
                            <th hidden="hidden">信息</th>
                            <th class="width200"></th>
                        </tr>                      
                        <c:forEach items="${tariffList}" var="c">
                        <tr>
                        	<td>${c.postageId}</td>
                        	<td>${c.postageName}</td>
                        	<td>${c.basicTime}</td>
                        	<td>${c.basicCost}</td>
                        	<td>${c.unitCost}</td>
                        	<td>${c.creationTime}</td>
                        	<td>${c.openTime}</td>
                        	<td>${c.state}</td>
                            <td hidden="hidden">${c.information}</td>   
                            <td>   
                            <c:if test="${ c.state=='暂停'}">                         
                                <input type="button" value="启用" class="btn_start" onclick="startFee(this);" />
                                <input type="button" value="修改" class="btn_modify" onclick="location.href='${pageContext.request.contextPath}/admin/fee.do?click=3&id=${c.postageId }'"/>
                                <input type="button" value="删除" class="btn_delete" onclick="deletePostage(this);"/>
                             </c:if> 
                             <c:if test="${ c.state=='开通'}">                         
                                <input type="button" value="暂停" class="btn_start" onclick="startFee(this);" />
                                <input type="button" value="修改" class="btn_modify" onclick="location.href='${pageContext.request.contextPath}/admin/fee.do?click=3&id=${c.postageId }'" disabled="disabled"/>
                                <input type="button" value="删除" class="btn_delete" onclick="deletePostage(this);" disabled="disabled"/>
                             </c:if>
                                
                            </td>
                        </tr>
                        </c:forEach>
                    </table>
                    <p>业务说明：<br />
                    1、创建资费时，状态为暂停，记载创建时间；<br />
                    2、暂停状态下，可修改，可删除；<br />
                    3、开通后，记载开通时间，且开通后不能修改、不能再停用、也不能删除；<br />
                    4、业务账号修改资费时，在下月底统一触发，修改其关联的资费ID（此触发动作由程序处理）
                    </p>
                </div>
                <!--分页-->
                <div id="pages">
                <c:if test="${page.curPage>1}">
        	        <a href="../admin/showView.do?click=4&curPage=${page.curPage-1}">上一页</a>
        	        </c:if>
                    <c:if test="${page.curPage<page.totalPage}">
                    <a href="../admin/showView.do?click=4&curPage=${page.curPage+1}">下一页</a>
                    </c:if>
                </div>
            </form>
        </div>
        <!--主要区域结束-->
        <div id="footer">
           
        </div>
        <script>
        	function(){
        	if(${deleteMessage}!=null)
        		showResult();
        	}
        </script>
    </body>
</html>
