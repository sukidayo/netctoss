<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global.css" />
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global_color.css" /> 
       	<style type="text/css">
       		#bt{width:126px;height:42px;border-width: 0px;}
      	 </style>
    </head>
    <body class="index">
	    <form action="${pageContext.request.contextPath }/admin/login.do" >
	        <div class="login_box">
	            <table>
	                <tr>
	                    <td class="login_info">账号：</td>
	                    <td colspan="2"><input name="account" type="text" value="${admin.account}" class="width150" /></td>
	                    <td class="login_error_info"><span class="required"></span></td>
	                </tr>
	                <tr>
	                    <td class="login_info">密码：</td>
	                    <td colspan="2"><input name="password" type="password" value="${admin.password}" class="width150" /></td>
	                    <td><span class="required"></span></td>
	                </tr>
	                <tr>
	                    <td class="login_info">验证码：</td>
	                    <td class="width70"><input name="" type="text" class="width70" /></td>
	                    <td><img src="../images/valicode.jpg" alt="验证码" title="点击更换" /></td>  
	                    <td><span class="required"></span></td>              
	                </tr>       
	                <tr>
	                    <td></td>
	                    <td class="login_button" colspan="2">
	                        <input id="bt" type="image" src="../images/login_btn.png" name="submit"/> 
	                        <font color="red">${errorMsg }</font>
	                    </td>    
	                    <td><span class="required" name="loginerror"></span></td>                
	                </tr>
	            </table>
	        </div>
       </form> 
    </body>
</html>