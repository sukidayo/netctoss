<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.7.2.min.js"></script>
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global.css" />
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global_color.css" />
        <script language="javascript" type="text/javascript">
        
        	//删除消息提示
        	 function showResult() {
                showResultDiv(true);
                window.setTimeout("showResultDiv(false);", 3000);
            }
        	//修改消息提示
        	 function showResult1() {
                showResultDiv1(true);
                window.setTimeout("showResultDiv1(false);", 3000);
            }
            function showResultDiv(flag) {
                var divResult = document.getElementById("save_result_info");
                if (flag)
                    divResult.style.display = "block";
                else
                    divResult.style.display = "none";
            }
            function showResultDiv1(flag) {
                var divResult = document.getElementById("change_result_info");
                if (flag)
                    divResult.style.display = "block";
                else
                    divResult.style.display = "none";
            }
            
            //显示角色详细信息
            function showDetail(flag, a) {
                var detailDiv = a.parentNode.getElementsByTagName("div")[0];
                if (flag) {
                    detailDiv.style.display = "block";
                }
                else
                    detailDiv.style.display = "none";
            }
            //删除
            function deleteBusiness(id){
            	var r = window.confirm("确定要删除此业务吗？");
            	var rows = id.parentNode.parentNode.rowIndex;
            	var table = document.getElementById("datalist");
            	var pid = table.rows[rows].cells[0].innerHTML;
            	location.href='${pageContext.request.contextPath}/business/delete.do?id='+pid
            }
            //开通或暂停
            function setState(id) {
            	var rows = id.parentNode.parentNode.rowIndex;
            	var table = document.getElementById("datalist");
            	var pid = table.rows[rows].cells[0].innerHTML;
            	var state = id.value
            	if("暂停"==state){
            		 window.confirm("确定要暂停此业务账号吗？");
            		location.href='${pageContext.request.contextPath}/business/state.do?state=1&id='+pid
            	}
            	else{
            		 window.confirm("确定要开通此业务账号吗？");
            		 location.href='${pageContext.request.contextPath}/business/state.do?state=2&id='+pid
            	}
               
     
              
                /* location.href='${pageContext.request.contextPath}/business/delete.do?id='+pid */
            }
        </script>
    </head>
    <body>
        <!--Logo区域开始-->
        <div id="header">
            <img src="../images/logo.png" alt="logo" class="left"/>
            <span>当前账号：<b>${currentAdmin.account}</b></span>
            <a href="${pageContext.request.contextPath }/admin/showlogin.do">[退出]</a>            
        </div>
        <!--Logo区域结束-->
        <!--导航区域开始-->
        <div id="navi">
	    	<ul id="menu">
		        <li><a href="../admin/showView.do?click=1" class="index_off"></a></li>
		        <li><a href="../admin/showView.do?click=2" class="role_off"></a></li>
		        <li><a href="../admin/showView.do?click=3" class="admin_off"></a></li>
		        <li><a href="../admin/showView.do?click=4" class="fee_off"></a></li>
		        <li><a href="../admin/showView.do?click=5" class="account_off"></a></li>
		        <li><a href="../admin/showView.do?click=6" class="service_on"></a></li>
		        <li><a href="../admin/showView.do?click=7" class="bill_off"></a></li>
		        <li><a href="../admin/showView.do?click=8" class="report_off"></a></li>
		        <li><a href="../admin/showView.do?click=9" class="information_off"></a></li>
		        <li><a href="../admin/showView.do?click=10" class="password_off"></a></li>
	    	</ul>
		</div>
        <!--导航区域结束-->
        <!--主要区域开始-->
        <div id="main">
            <form action="${pageContext.request.contextPath }/admin/showView.do?click=6" method="post">
                <!--查询-->
                <div class="search_add">                        
                    <div>OS 账号：<input type="text" value="" class="width100 text_search" name="osNumber"/></div>                            
                    <div>服务器 IP：<input type="text" value="" class="width100 text_search" name="serverIP"/></div>
                    <div>身份证：<input type="text"  value="" class="text_search"  name="idNumber"/></div>
                    <div>状态：
                        <select class="select_search" name="state">
                            <option>全部</option>
                            <option>开通</option>
                            <option>暂停</option>
                            <option>删除</option>
                        </select>
                    </div>
                    <div><input type="submit" value="搜索" class="btn_search" /></div>
                    <input type="button" value="增加" class="btn_add" onclick="location.href='${pageContext.request.contextPath }/admin/service.do?click=1&id=0';" />
                </div>  
                 <!--保存操作的提示信息-->
            <div id="save_result_info" class="save_success">${deleteBusinessMessage}</div>
             <div id="change_result_info" class="save_fail">${changeMessage}</div>
                <!--数据区域：用表格展示数据-->     
                <div id="data">            
                    <table id="datalist">
                    <tr>
                        <th class="width50">业务ID</th>
                        <th class="width70">账务账号ID</th>
                        <th class="width150">身份证</th>
                        <th class="width70">姓名</th>
                        <th>OS 账号</th>
                        <th class="width50">状态</th>
                        <th class="width100">服务器 IP</th>
                        <th class="width100">资费</th>                                                        
                        <th class="width200"></th>
                    </tr>
                    <c:forEach items="${businessList}" var="c">
                    <tr>
                        <td>${c.businessID}</td>
                        <td>${c.accountID}</td>
                        <td>${c.idNumber}</td>
                        <td>${c.userName}</td>
                        <td>${c.osNumber}</td>
                        <td>${c.state}</td>
                        <td>${c.serverIP}</td>
                        <td>
                            <a class="summary"  onmouseover="showDetail(true,this);" onmouseout="showDetail(false,this);">${c.postageName}</a>
                            <!--浮动的详细信息-->
                            <div class="detail_info">
                                ${c.postage.basicTime}，${c.postage.basicCost } 元，超出部分 0.03元/分钟
                            </div>
                        </td>                            
                        <td class="td_modi">
                        <c:if test="${c.state=='开通' }">
                            <input type="button" value="暂停" class="btn_pause" onclick="setState(this);" />
                            <input type="button" value="修改" class="btn_modify" onclick="location.href='${pageContext.request.contextPath }/admin/service.do?click=3&id=${c.businessID }';" disabled="disabled"/>
                            <input type="button" value="删除" class="btn_delete" onclick="deleteBusiness(this);" disabled="disabled"/>
                            </c:if>
                            <c:if test="${c.state=='暂停' }">
                            <input type="button" value="开通" class="btn_pause" onclick="setState(this);" />
                            <input type="button" value="修改" class="btn_modify" onclick="location.href='${pageContext.request.contextPath }/admin/service.do?click=3&id=${c.businessID }';"/>
                            <input type="button" value="删除" class="btn_delete" onclick="deleteBusiness(this);"/>
                            </c:if>
                        </td>
                    </tr>
                    </c:forEach>                                                            
                </table>                
                <p>业务说明：<br />
                1、创建即开通，记载创建时间；<br />
                2、暂停后，记载暂停时间；<br />
                3、重新开通后，删除暂停时间；<br />
                4、删除后，记载删除时间，标示为删除，不能再开通、修改、删除；<br />
                5、业务账号不设计修改密码功能，由用户自服务功能实现；<br />
                6、暂停和删除状态的账务账号下属的业务账号不能被开通。</p>
                </div>                    
                <!--分页-->
                <div id="pages">
                    <a href="${pageContext.request.contextPath }/admin/showView.do?click=6&curPage=1">首页</a>
        	        <c:if test="${page.curPage>1}">
        	        <a href="${pageContext.request.contextPath }/admin/showView.do?click=6&curPage=${page.curPage-1}">上一页</a>
        	        </c:if>
                    <c:if test="${page.curPage<page.totalPage}">
                    <a href="${pageContext.request.contextPath }/admin/showView.do?click=6&curPage=${page.curPage+1}">下一页</a>
                    </c:if>
                    <a href="${pageContext.request.contextPath }/admin/showView.do?click=6&curPage=${page.totalPage}">末页</a>
                </div>                    
            </form>
        </div>
        <!--主要区域结束-->
        <div id="footer">
           
        </div>
        <script>
        showResult();
        showResult1();
        </script>
    </body>
</html>
