<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link type="text/css" rel="stylesheet" media="all" href="../styles/global.css" />
<link type="text/css" rel="stylesheet" media="all" href="../styles/global_color.css" />
<script language="javascript" type="text/javascript">
	//显示角色详细信息
	function showDetail(flag, a) {
		var detailDiv = a.parentNode.getElementsByTagName("div")[0];
		if (flag) {
			detailDiv.style.display = "block";
		} else
			detailDiv.style.display = "none";
	} 
	//删除
	function deleteAdmin(aid) {
		var r = window.confirm("确定要删除此管理员吗？");			
		if(r == true){
			window.location.href='${pageContext.request.contextPath }/admin/deleteAdmin.do?aid='+aid;			
		}else{
			document.getElementById("operate_result_info").style.display = "block";			
		}
	}
	//全选
	function selectAdmins(inputObj) {
		var inputArray = document.getElementById("datalist")
				.getElementsByTagName("input");
		for (var i = 1; i < inputArray.length; i++) {
			if (inputArray[i].type == "checkbox") {
				inputArray[i].checked = inputObj.checked;
			}
		}
	}
</script>
</head>
<body>
	<!--Logo区域开始-->
	<div id="header">
		<img src="../images/logo.png" alt="logo" class="left" /> <span>当前账号：<b>${currentAdmin.account}</b></span>
		<a href="${pageContext.request.contextPath }/admin/showlogin.do">[退出]</a>
	</div>
	<!--Logo区域结束-->
	<!--导航区域开始-->
	<div id="navi">
		<ul id="menu">
			<li><a href="../admin/showView.do?click=1" class="index_off"></a></li>
			<li><a href="../admin/showView.do?click=2" class="role_off"></a></li>
			<li><a href="../admin/showView.do?click=3" class="admin_on"></a></li>
			<li><a href="../admin/showView.do?click=4" class="fee_off"></a></li>
			<li><a href="../admin/showView.do?click=5" class="account_off"></a></li>
			<li><a href="../admin/showView.do?click=6" class="service_off"></a></li>
			<li><a href="../admin/showView.do?click=7" class="bill_off"></a></li>
			<li><a href="../admin/showView.do?click=8" class="report_off"></a></li>
			<li><a href="../admin/showView.do?click=9" class="information_off"></a></li>
			<li><a href="../admin/showView.do?click=10" class="password_off"></a></li>
		</ul>
	</div>
	<!--导航区域结束-->
	<!--主要区域开始-->
	<div id="main">	
		<!--查询-->
		<div class="search_add">
			<div>
				模块： <select id="selModules" class="select_search">
					<option>全部</option>
					<option>角色管理</option>
					<option>管理员管理</option>
					<option>资费管理</option>
					<option>账务账号</option>
					<option>业务账号</option>
					<option>账单管理</option>
					<option>报表</option>
				</select> 
			</div>
			<form action="${pageContext.request.contextPath }/admin/selectAdminByCondition.do" method="get">
			<div>
				角色：<input type="text" name="privil" class="text_search width200" />
			</div>
			<div>
				<input type="submit" value="搜索" class="btn_search" />
			</div>
			</form>
			<!-- <input type="button" value="密码重置" class="btn_add" onclick="resetPwd();" /> -->
			<input type="button" value="增加" class="btn_add"
				onclick="location.href='${pageContext.request.contextPath }/admin/adminView.do?click=1';" />
		</div>
		<!--删除和密码重置的操作提示-->
		<div id="operate_result_info" class="operate_fail">
			<img src="../images/close.png" onclick="this.parentNode.style.display='none';" /> 
			删除操作失败！
		</div>
		<!--数据区域：用表格展示数据-->
		<div id="data">
			<table id="datalist">
				<tr>
					<th class="th_select_all"><input type="checkbox"
						onclick="selectAdmins(this);" /> <span>全选</span></th>
					<th>管理员ID</th>
					<th>姓名</th>
					<th>登录名</th>
					<th>电话</th>
					<th>电子邮件</th>
					<th>授权日期</th>
					<th class="width100">拥有权限</th>
					<th></th>
				</tr>
				<c:forEach items="${adminsInfor}" var="admin">
					<tr>
						<td><input type="checkbox" name="details"/></td>
						<td id="aid">${admin.aid}</td>
						<td>${admin.aname}</td>
						<td>${admin.account}</td>
						<td>${admin.aphone}</td>
						<td>${admin.aemail}</td>
						<td>2018/10/5</td>
						<td><a class="summary" onmouseover="showDetail(true,this);"
							onmouseout="showDetail(false,this);">个人权限</a> <!--浮动的详细信息-->
							<div class="detail_info">${admin.privil}</div></td>
						<td class="td_modi"><input type="button" value="修改" class="btn_modify"
							onclick="location.href='${pageContext.request.contextPath }/admin/adminView.do?click=2&aid=${admin.aid}&aname=${admin.aname}&account=${admin.account}&aphone=${admin.aphone}&aemail=${admin.aemail}';" />
							<input type="button" value="删除" class="btn_delete" onclick="deleteAdmin(${admin.aid});"/>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<!--分页-->
		<div id="pages">
			<a href="#">上一页</a> <a href="#" class="current_page">1</a> <a
				href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a href="#">5</a>
			<a href="#">下一页</a>
		</div>
	</div>
	<!--主要区域结束-->
	<div id="footer"></div>
</body>
</html>

