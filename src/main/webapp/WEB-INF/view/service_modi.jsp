<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global.css" />
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global_color.css" /> 
        <script language="javascript" type="text/javascript">
            //保存成功的提示信息
            function showResult() {
                showResultDiv(true);
                window.setTimeout("showResultDiv(false);", 3000);
            }
            function showResultDiv(flag) {
                var divResult = document.getElementById("save_result_info");
                if (flag)
                    divResult.style.display = "block";
                else
                    divResult.style.display = "none";
            }
        </script>
    </head>
    <body>
        <!--Logo区域开始-->
        <div id="header">
            <img src="../images/logo.png" alt="logo" class="left"/>
            <span>当前账号：<b>${account}</b></span>
            <a href="${pageContext.request.contextPath }/admin/showlogin.do">[退出]</a>            
        </div>
        <!--Logo区域结束-->
        <!--导航区域开始-->
        <div id="navi">
	    	<ul id="menu">
		        <li><a href="../admin/showView.do?click=1" class="index_off"></a></li>
		        <li><a href="../admin/showView.do?click=2" class="role_off"></a></li>
		        <li><a href="../admin/showView.do?click=3" class="admin_off"></a></li>
		        <li><a href="../admin/showView.do?click=4" class="fee_off"></a></li>
		        <li><a href="../admin/showView.do?click=5" class="account_off"></a></li>
		        <li><a href="../admin/showView.do?click=6" class="service_on"></a></li>
		        <li><a href="../admin/showView.do?click=7" class="bill_off"></a></li>
		        <li><a href="../admin/showView.do?click=8" class="report_off"></a></li>
		        <li><a href="../admin/showView.do?click=9" class="information_off"></a></li>
		        <li><a href="../admin/showView.do?click=10" class="password_off"></a></li>
	    	</ul>
		</div>
        <!--导航区域结束-->
        <!--主要区域开始-->
        <div id="main">
            <!--保存操作的提示信息-->
            <div id="save_result_info" class="save_success">${message}</div>
            <form action="${pageContext.request.contextPath }/business/modi.do" method="post" class="main_form">
                <!--必填项-->
                <div class="text_info clearfix"><span>业务账号ID：</span></div>
                <div class="input_info">
                    <input type="text" value="${b.businessID }" readonly class="readonly" name="businessID"/>
                </div>
                <div class="text_info clearfix"><span>OS 账号：</span></div>
                <div class="input_info">
                    <input type="text" value="${b.osNumber }" readonly class="readonly" name="osNumber"/>
                </div>
                <div class="text_info clearfix"><span>服务器 IP：</span></div>
                <div class="input_info">
                    <input type="text" value="${b.serverIP }" readonly class="readonly" name="serverIP"/>
                </div>
                <div class="text_info clearfix"><span>资费类型：</span></div>
                <div class="input_info">
                    <select class="width150" name="postageName">
                        <option>${b.postageName}</option>
                        <c:forEach items="${p }" var="p">
                        <option>${p.postageName }</option>
                        </c:forEach>
                    </select>
                    <div class="validate_msg_long">请修改资费类型，或者取消修改操作。</div>                      
                </div>
                <!--操作按钮-->
                <div class="button_info clearfix">
                    <input type="submit" value="保存" class="btn_save"/>
                    <input type="button" value="取消" class="btn_save" onclick="location.href='${pageContext.request.contextPath }/admin/showView.do?click=6';"/>
                </div>

                
                <p>业务说明：<br />
                1、修改资费后，点击保存，并未真正修改数据库中的数据；<br />
                2、提交操作到消息队列；<br />
                3、每月月底由程序自动完成业务所关联的资费；</p>
                
            </form>  
        </div>
        <!--主要区域结束-->
        <div id="footer">
           
        </div>
        <script>showResult();</script>
    </body>
</html>
