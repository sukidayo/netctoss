<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global.css" />
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global_color.css" />  
        
        <script language="javascript" type="text/javascript">
            //写入下拉框中的年份和月份
            function initialYearAndMonth() {
                //写入最近3年
                var yearObj = document.getElementById("selYears");
                var year = (new Date()).getFullYear();
                for (var i = 0; i <= 5; i++) {
                    var opObj = new Option(year - i, year - i);
                    yearObj.options[i] = opObj;
                }
                //写入 12 月
                var monthObj = document.getElementById("selMonths");
                var opObj = new Option("全部", "全部");
                monthObj.options[0] = opObj;
                for (var i = 1; i <= 12; i++) {
                    var opObj = new Option(i, i);
                    monthObj.options[i] = opObj;
                }
            }
        </script>
    </head>
    <body onload="initialYearAndMonth();">
        <!--Logo区域开始-->
        <div id="header">
            <img src="../images/logo.png" alt="logo" class="left"/>
            <span>当前账号：<b>${currentAdmin.account}</b></span>
            <a href="${pageContext.request.contextPath }/admin/showlogin.do">[退出]</a>            
        </div>
        <!--Logo区域结束-->
        <!--导航区域开始-->
        <div id="navi">
	    	<ul id="menu">
		        <li><a href="../admin/showView.do?click=1" class="index_off"></a></li>
		        <li><a href="../admin/showView.do?click=2" class="role_off"></a></li>
		        <li><a href="../admin/showView.do?click=3" class="admin_off"></a></li>
		        <li><a href="../admin/showView.do?click=4" class="fee_off"></a></li>
		        <li><a href="../admin/showView.do?click=5" class="account_off"></a></li>
		        <li><a href="../admin/showView.do?click=6" class="service_off"></a></li>
		        <li><a href="../admin/showView.do?click=7" class="bill_on"></a></li>
		        <li><a href="../admin/showView.do?click=8" class="report_off"></a></li>
		        <li><a href="../admin/showView.do?click=9" class="information_off"></a></li>
		        <li><a href="../admin/showView.do?click=10" class="password_off"></a></li>
	    	</ul>
		</div>
        <!--导航区域结束-->
        <!--主要区域开始-->
        <div id="main">
            <form action="bill_list.do" method="get">
                <!--查询-->
                <div class="search_add">                        
                    <div>身份证：<input type="text" name="idNumber" value="" class="text_search" /></div>
                    <div>账务账号：<input type="text" name="accountNumber" value="" class="width100 text_search" /></div>                            
                    <div>姓名：<input type="text" name="userName" value="" class="width70 text_search" /></div>
      			<!-- <div>
                        <select class="select_search" id="selYears">
                        </select>年
                        <select class="select_search" name="month" id="selMonths">
                        </select>月
                    </div> -->
                    <div><input type="submit" value="搜索" class="btn_search" /></div>
                </div>  
                <!--数据区域：用表格展示数据-->     
                <div id="data">            
                    <table id="datalist">
                    <tr>
                        <th class="width50">账单ID</th>
                        <th class="width70">姓名</th>
                        <th class="width150">身份证</th>
                        <th class="width150">账务账号</th>
                        <th>费用</th>
                        <th class="width100">月份</th>
                        <th class="width100">支付方式</th>
                        <th class="width100">支付状态</th>                                                        
                        <th class="width50"></th>
                    </tr>
     
            	<c:forEach items="${bill}" var="bill">
                    <tr>
                        <td>${bill.billID}</td>  
                        <td>${bill.userName}</td>
                        <td>${bill.idNumber}</td>
                        <td>${bill.accountNumber}</td>
                        <td>${bill.cost}</td>
                        <td><fmt:formatDate value="${bill.paymentTime}" type="date" pattern="yyyy年MM月" dateStyle="default"/></td>
                        <td>${bill.paymentMethod}</td>   
                        <td>${bill.paymentState}</td>  
                        <td><a href="${pageContext.request.contextPath }/admin/bill.do?click=1&billID=${bill.billID}&accountNumber=${bill.accountNumber}" title="账单明细">明细</a></td>                           
                    </tr>
              	</c:forEach>     
                 
                </table>
                
             <!--    <p>业务说明：<br />
                1、设计支付方式和支付状态，为用户自服务中的支付功能预留；<br />
                2、只查询近 3 年的账单，即当前年和前两年，如2013/2012/2011；<br />
                3、年和月的数据由 js 代码自动生成；<br />
                4、由数据库中的ｊｏｂ每月的月底定时计算账单数据。</p> -->
                </div>                    
                <!--分页-->
                <div id="pages">
                <c:choose>
	                <c:when test="${page.curPage>1}">
	        	    	<a href="../admin/showView.do?click=7&curPage=${page.curPage-1}">上一页</a>
	        	    </c:when >
	        	    <c:otherwise><p style="display: inline-block;padding:8px 12px;">上一页</p></c:otherwise>
	        	</c:choose>
	        	
	        	<c:forEach items="${curPageNum}" var="curPage">
	        		<a href="../admin/showView.do?click=7&curPage=${curPage+1}">${curPage+1}</a>
	        	</c:forEach> 
	        	
	        	<c:choose>   	    
	                <c:when test="${page.curPage<page.totalPage}">
	                	<a href="../admin/showView.do?click=7&curPage=${page.curPage+1}">下一页</a>
	                </c:when >
	        	    <c:otherwise><p style="display: inline-block;padding:8px 12px;">下一页</p></c:otherwise>
	        	</c:choose>
                </div>       
            </form>
        </div>
        <!--主要区域结束-->
        <div id="footer">
            
           
        </div>
    </body>
</html>
