<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% String base=request.getContextPath()+"/";%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global.css" />
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global_color.css" /> 
        <script language="javascript" type="text/javascript">
            //保存成功的提示消息
            function showResult() {
                showResultDiv(true);
                window.setTimeout("showResultDiv(false);", 3000);
            }
            function showResultDiv(flag) {
                var divResult = document.getElementById("save_result_info");
                if (flag)
                    divResult.style.display = "block";
                else
                    divResult.style.display = "none";
            }
        </script>
    </head>
    <body>
        <!--Logo区域开始-->
        <div id="header">
            <img src="../images/logo.png" alt="logo" class="left"/>
            <span>当前账号：<b>${currentAdmin.account}</b></span>
            <a href="${pageContext.request.contextPath }/admin/showlogin.do">[退出]</a>            
        </div>
        <!--Logo区域结束-->
        <!--导航区域开始-->
        <div id="navi">
	    	<ul id="menu">
		        <li><a href="../admin/showView.do?click=1" class="index_off"></a></li>
		        <li><a href="../admin/showView.do?click=2" class="role_off"></a></li>
		        <li><a href="../admin/showView.do?click=3" class="admin_on"></a></li>
		        <li><a href="../admin/showView.do?click=4" class="fee_off"></a></li>
		        <li><a href="../admin/showView.do?click=5" class="account_off"></a></li>
		        <li><a href="../admin/showView.do?click=6" class="service_off"></a></li>
		        <li><a href="../admin/showView.do?click=7" class="bill_off"></a></li>
		        <li><a href="../admin/showView.do?click=8" class="report_off"></a></li>
		        <li><a href="../admin/showView.do?click=9" class="information_off"></a></li>
		        <li><a href="../admin/showView.do?click=10" class="password_off"></a></li>
	    	</ul>
		</div>
        <!--导航区域结束-->
        <!--主要区域开始-->
        <div id="main">            
            <div id="save_result_info" class="save_success">${uflag}</div>           
            <form action="${pageContext.request.contextPath }/admin/updateAdmin.do" method="get" class="main_form">
            <div style="display:none">
				<input type="text" value="${admin.aid}" name="aid"/>
			</div>
	            <div class="text_info clearfix"><span>姓名：</span></div>
	            <div class="input_info">
	                <input type="text" value="${admin.aname}" name="aname"/>	                
	                <!-- <span class="required">*</span>
	                <div class="validate_msg_long error_msg">20长度以内的汉字、字母、数字的组合</div> -->
	            </div>
	            <div class="text_info clearfix"><span>管理员账号：</span></div>
	            <div class="input_info"><input type="text" readonly="readonly" class="readonly" value="${admin.account }" name="account" /></div>
	            <div class="text_info clearfix"><span>电话：</span></div>
	            <div class="input_info">
	                <input type="text" value="${admin.aphone }" name="aphone"/>
	                <!-- <span class="required">*</span>
	                <div class="validate_msg_long error_msg">正确的电话号码格式：手机或固话</div> -->
	            </div>
	            <div class="text_info clearfix"><span>Email：</span></div>
	            <div class="input_info">
	                <input type="text" class="width200" value="${admin.aemail }" name="aemail"/>
	                <!-- <span class="required">*</span>
	                <div class="validate_msg_medium error_msg">50长度以内，正确的 email 格式</div> -->
	            </div>
	            <div class="text_info clearfix"><span>角色：</span></div>
	            <div class="input_info_high">
	                <div class="input_info_scroll">
	                    <ul>
	                        <li><input type="checkbox" name="pname" value="2"/>管理员管理</li>
                            <li><input type="checkbox" name="pname" value="3"/>资费管理</li>
                            <li><input type="checkbox" name="pname" value="4"/>账务账号</li>
                            <li><input type="checkbox" name="pname" value="5"/>业务账号</li>
                            <li><input type="checkbox" name="pname" value="6"/>账单管理</li>
                            <li><input type="checkbox" name="pname" value="7"/>报表</li>
                            <li><input type="checkbox" name="pname" value="1"/>角色管理</li>
	                    </ul>
	                </div>
	            </div>
	            <div class="button_info clearfix">
	                <input type="submit" value="保存" class="btn_save"/>
	                <input type="button" value="取消" class="btn_save" onclick="location.href='${pageContext.request.contextPath }/admin/showView.do?click=3';"/>
	            </div>
	            
	        </form>  
        </div>
        <!--主要区域结束-->
        <div id="footer">           
        </div>
        <script>showResult()</script>
    </body>
</html>
