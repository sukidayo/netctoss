<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global.css" />
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global_color.css" /> 
    </head>
    <body onload="initialYearAndMonth();">
        <!--Logo区域开始-->
        <div id="header">
            <img src="../images/logo.png" alt="logo" class="left"/>
            <span>当前账号：<b>${account}</b></span>
            <a href="${pageContext.request.contextPath }/admin/showlogin.do">[退出]</a>            
        </div>
        <!--Logo区域结束-->
        <!--导航区域开始-->
        <div id="navi">
	    	<ul id="menu">
		        <li><a href="../admin/showView.do?click=1" class="index_off"></a></li>
		        <li><a href="../admin/showView.do?click=2" class="role_off"></a></li>
		        <li><a href="../admin/showView.do?click=3" class="admin_off"></a></li>
		        <li><a href="../admin/showView.do?click=4" class="fee_off"></a></li>
		        <li><a href="../admin/showView.do?click=5" class="account_off"></a></li>
		        <li><a href="../admin/showView.do?click=6" class="service_off"></a></li>
		        <li><a href="../admin/showView.do?click=7" class="bill_on"></a></li>
		        <li><a href="../admin/showView.do?click=8" class="report_off"></a></li>
		        <li><a href="../admin/showView.do?click=9" class="information_off"></a></li>
		        <li><a href="../admin/showView.do?click=10" class="password_off"></a></li>
	    	</ul>
		</div>
        <!--导航区域结束-->
        <!--主要区域开始-->
        <div id="main">
            <form action="bill_item.do" method="">
                <!--查询-->
                <div class="search_add">                        
                    <div>账务账号：<span class="readonly width70">${bill.accountNumber}</span></div>                            
                    <div>身份证：<span class="readonly width150">${bill.idNumber}</span></div>
                    <div>姓名：<span class="readonly width70">${bill.userName}</span></div>
                    <div>计费时间：<span class="readonly width70"><fmt:formatDate value="${bill.paymentTime}" type="date" pattern="yyyy年MM月" dateStyle="default"/></span></div>
                    <div>总费用：<span class="readonly width70">${bill.cost}</span></div>
                    <input type="button" value="返回" class="btn_add" onclick="location.href='${pageContext.request.contextPath }/admin/showView.do?click=7';" />
                </div>  
                <!--数据区域：用表格展示数据-->     
                <div id="data">            
                    <table id="datalist">
                        <tr>
                            <th class="width70">账单明细ID</th>
                            <th class="width150">OS 账号</th>
                            <th class="width150">服务器 IP</th>
                            <th class="width70">账务账号ID</th>
                            <th class="width150">时长</th>
                            <th>费用</th>
                            <th class="width150">资费</th>
                            <th class="width50"></th>
                        </tr>
                        
                        <c:forEach items="${detail}" var="detail">
		                    <tr>
		                        <td>${detail.detailedID}</td>  
		                        <td>${detail.osNumber}</td>
		                        <td>${detail.serverIP}</td>
		                        <td>${detail.accountNumber}</td>
		                        <td>${detail.lengthTime}</td>
		                        <td>${detail.basicCost}</td>
		                        <td>${detail.postageName}</td>   
		                        <td><a href="${pageContext.request.contextPath }/admin/bill.do?click=2&billID=${billID}&serverIP=${detail.serverIP}&accountNumber=${detail.accountNumber}" title="业务详单">详单</a></td>
		                    	
		                    </tr>
		              	</c:forEach>
                        
                    
                        <%-- <tr>
                            <td>2</td>
                            <td>openlab1</td>
                            <td>192.168.100.20</td>
                            <td>101</td>
                            <td>3分15秒</td>
                            <td>3.45</td>
                            <td>包 20 小时</td>                          
                            <td><a href="${pageContext.request.contextPath }/admin/bill.do?click=2" title="业务详单">详单</a></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>openlab1</td>
                            <td>192.168.0.23</td>
                            <td>101</td>
                            <td>13分15秒</td>
                            <td>13.45</td>
                            <td>包 40 小时</td>                          
                            <td><a href="${pageContext.request.contextPath }/admin/bill.do?click=2" title="业务详单">详单</a></td>
                        </tr> --%>
                    </table>
                </div>
                <!--分页-->
               <div id="pages">
                <c:choose>
	                <c:when test="${page.curPage>1}">
	        	    	<a href="${pageContext.request.contextPath }/admin/bill.do?click=1&billID=${billID}&accountNumber=${bill.accountNumber}&curPage=${page.curPage-1}">上一页</a>
	        	    </c:when >
	        	    <c:otherwise><p style="display: inline-block;padding:8px 12px;">上一页</p></c:otherwise>
	        	</c:choose>
	        	
	        	<c:forEach items="${curPageNum}" var="curPage">
	        		<a href="${pageContext.request.contextPath }/admin/bill.do?click=1&billID=${billID}&accountNumber=${bill.accountNumber}&curPage=${curPage+1}">${curPage+1}</a>
	        	</c:forEach> 
	        	
	        	<c:choose>   	    
	                <c:when test="${page.curPage<page.totalPage}">
	                	<a href="${pageContext.request.contextPath }/admin/bill.do?click=1&billID=${billID}&accountNumber=${bill.accountNumber}&curPage=${page.curPage+1}">下一页</a>
	                </c:when >
	        	    <c:otherwise><p style="display: inline-block;padding:8px 12px;">下一页</p></c:otherwise>
	        	</c:choose>
                </div>                
            </form>
        </div>
        <!--主要区域结束-->
        <div id="footer">
          
        </div>
    </body>
</html>
