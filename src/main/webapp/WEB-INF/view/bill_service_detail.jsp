<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global.css" />
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global_color.css" />  
    </head>
    <body onload="initialYearAndMonth();">
        <!--Logo区域开始-->
        <div id="header">
            <img src="../images/logo.png" alt="logo" class="left"/>
            <span>当前账号：<b>${account}</b></span>
            <a href="${pageContext.request.contextPath }/admin/showlogin.do">[退出]</a>            
        </div>
        <!--Logo区域结束-->
        <!--导航区域开始-->
        <div id="navi">
	    	<ul id="menu">
		        <li><a href="../admin/showView.do?click=1" class="index_off"></a></li>
		        <li><a href="../admin/showView.do?click=2" class="role_off"></a></li>
		        <li><a href="../admin/showView.do?click=3" class="admin_off"></a></li>
		        <li><a href="../admin/showView.do?click=4" class="fee_off"></a></li>
		        <li><a href="../admin/showView.do?click=5" class="account_off"></a></li>
		        <li><a href="../admin/showView.do?click=6" class="service_off"></a></li>
		        <li><a href="../admin/showView.do?click=7" class="bill_on"></a></li>
		        <li><a href="../admin/showView.do?click=8" class="report_off"></a></li>
		        <li><a href="../admin/showView.do?click=9" class="information_off"></a></li>
		        <li><a href="../admin/showView.do?click=10" class="password_off"></a></li>
	    	</ul>
		</div>
        <!--导航区域结束-->
        <!--主要区域开始-->
        <div id="main">
            <form action="" method="">
                <!--查询-->
                <div class="search_add">                        
                    <div>账务账号：<span class="readonly width70">${detail.accountNumber}</span></div>                            
                    <div>OS 账号：<span class="readonly width100">${detail.osNumber}</span></div>
                    <div>服务器 IP：<span class="readonly width100">${detail.serverIP}</span></div>
                    <div>时长：<span class="readonly width70">${detail.lengthTime}</span></div>
                    <div>费用：<span class="readonly width70">${detail.basicCost}</span></div>
                    <input type="button" value="返回" class="btn_add" onclick="location.href='${pageContext.request.contextPath }/admin/bill.do?click=1&billID=${billID}&accountNumber=${detail.accountNumber}';" />
                </div>  
                <!--数据区域：用表格展示数据-->     
                <div id="data">            
                    <table id="datalist">
                        <tr>
                            <th class="width150">客户登录 IP</th>
                            <th class="width150">登入时刻</th>
                            <th class="width150">登出时刻</th>
                            <th class="width100">时长（秒）</th>
                            <th class="width150">费用</th>
                            <th>资费</th>
                        </tr>
                        
                        <c:forEach items="${specificationList}" var="specification">
		                    <tr>
		                        <td>${specification.userLoginIp}</td>  
		                        <td>${specification.userLoginTime}</td> 
		                        <td>${specification.userExitTime}</td>  
		                        <%-- 
		                        <td><fmt:formatDate value="${specification.userLoginTime}" type="date" pattern="yyyy/MM/dd hh:mm:ss" dateStyle="default"/></td>
		                        <td><fmt:formatDate value="${specification.userExitTime}" type="date" pattern="yyyy/MM/dd hh:mm:ss" dateStyle="default"/></td>
		                        --%> 
		                        
		                        <td>${specification.time}</td>
		                        <td>${specification.cost}</td>
		                        <td>${specification.postageName}</td>         
		                    </tr>
		              	</c:forEach>
                        
                        <!-- <tr>
                            <td>192.168.100.100</td>
                            <td>2013/01/01 12:12:12</td>
                            <td>2013/01/01 12:12:22</td>
                            <td>10</td>
                            <td>0</td>
                            <td>包 20 小时</td>
                        </tr>
                        <tr>
                            <td>192.168.100.100</td>
                            <td>2013/01/01 12:12:12</td>
                            <td>2013/01/01 12:12:22</td>
                            <td>10</td>
                            <td>0</td>
                            <td>包 20 小时</td>
                        </tr>
                        <tr>
                            <td>192.168.100.100</td>
                            <td>2013/01/01 12:12:12</td>
                            <td>2013/01/01 12:12:22</td>
                            <td>10</td>
                            <td>0</td>
                            <td>包 20 小时</td>
                        </tr>
                        <tr>
                            <td>192.168.100.100</td>
                            <td>2013/01/01 12:12:12</td>
                            <td>2013/01/01 12:12:22</td>
                            <td>10</td>
                            <td>0.45</td>
                            <td>包 20 小时</td>
                        </tr> -->
                    </table>
                </div>
                <!--分页-->
               <div id="pages">
                <c:choose>
	                <c:when test="${page.curPage>1}">
	        	    	<a href="${pageContext.request.contextPath }/admin/bill.do?click=2&billID=${billID}&serverIP=${detail.serverIP}&accountNumber=${detail.accountNumber}&curPage=${page.curPage-1}">上一页</a>
	        	    </c:when >
	        	    <c:otherwise><p style="display: inline-block;padding:8px 12px;">上一页</p></c:otherwise>
	        	</c:choose>
	        	
	        	<c:forEach items="${curPageNum}" var="curPage">
	        		<a href="${pageContext.request.contextPath }/admin/bill.do?click=2&billID=${billID}&serverIP=${detail.serverIP}&accountNumber=${detail.accountNumber}&curPage=${curPage+1}">${curPage+1}</a>
	        	</c:forEach> 
	        	
	        	<c:choose>   	    
	                <c:when test="${page.curPage<page.totalPage}">
	                	<a href="${pageContext.request.contextPath }/admin/bill.do?click=2&billID=${billID}&serverIP=${detail.serverIP}&accountNumber=${detail.accountNumber}&curPage=${page.curPage+1}">下一页</a>
	                </c:when >
	        	    <c:otherwise><p style="display: inline-block;padding:8px 12px;">下一页</p></c:otherwise>
	        	</c:choose>
                </div>                     
            </form>
        </div>
        <!--主要区域结束-->
        <div id="footer">
           
        </div>
    </body>
</html>
