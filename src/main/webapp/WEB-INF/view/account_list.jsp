<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <script src="${pageContext.request.contextPath }/jquery-1.7.2.min.js"></script>
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global.css" />
        <link type="text/css" rel="stylesheet" media="all" href="../styles/global_color.css" /> 
        
        <script language="javascript" type="text/javascript">
        
            //删除
            function deleteAccount(accountID) {
                var r = window.confirm("确定要删除此账务账号吗？\r\n删除后将不能恢复，且会删除其下属的所有业务账号。");
                var rows = accountID.parentNode.parentNode.rowIndex;
            	var table = document.getElementById("datalist");
            	var pid = table.rows[rows].cells[0].innerHTML;
            	location.href='${pageContext.request.contextPath}/admin/deleteAccount.do?accountID='+pid
                document.getElementById("operate_result_info").style.display = "block";
            }
            //开通或暂停
            function setState(id) {
                var r = window.confirm("确定要开通此账务账号吗？");
                	var rows = id.parentNode.parentNode.rowIndex;
                    var table = document.getElementById("datalist");
                    var s = table.rows[rows].cells[4].innerHTML;
                    var pid = table.rows[rows].cells[0].innerHTML; 
                    a=s+pid;
                    location.href='${pageContext.request.contextPath}/admin/updateState.do?id='+a;
                   
                	document.getElementById("operate_result_info").style.display = "block";
            }
        </script>
        
        <script>
        	
        </script>
    </head>
    <body>
        <!--Logo区域开始-->
        <div id="header">
           <img src="../images/logo.png" alt="logo" class="left" />
           <span>当前账号：<b>${currentAdmin.account}</b></span>
            <a href="${pageContext.request.contextPath }/admin/showlogin.do">[退出]</a>            
        </div>
        <!--Logo区域结束-->
        <!--导航区域开始-->
        <div id="navi">
	    	<ul id="menu">
		        <li><a href="../admin/showView.do?click=1" class="index_off"></a></li>
		        <li><a href="../admin/showView.do?click=2" class="role_off"></a></li>
		        <li><a href="../admin/showView.do?click=3" class="admin_off"></a></li>
		        <li><a href="../admin/showView.do?click=4" class="fee_off"></a></li>
		        <li><a href="../admin/showView.do?click=5" class="account_on"></a></li>
		        <li><a href="../admin/showView.do?click=6" class="service_off"></a></li>
		        <li><a href="../admin/showView.do?click=7" class="bill_off"></a></li>
		        <li><a href="../admin/showView.do?click=8" class="report_off"></a></li>
		        <li><a href="../admin/showView.do?click=9" class="information_off"></a></li>
		        <li><a href="../admin/showView.do?click=10" class="password_off"></a></li>
	    	</ul>
		</div>
        <!--导航区域结束-->
        <!--主要区域开始-->
        <div id="main">
            <form action="account_list.do" method="get">
                <!--查询-->
                <div class="search_add">                        
                    <div>身份证：<input type="text" value="" class="text_search" name="idNumber"/></div>                            
                    <div>姓名：<input type="text" class="width70 text_search" value="" name="userName"/></div>
                    <div>登录名：<input type="text"  value="" class="text_search"" name="userAccount"/></div>
                    <div>
                        状态：
                        <select class="select_search" name="state">
                            <option></option>
                            <option>开通</option>
                            <option>暂停</option>
                            <option>删除</option>
                        </select>
                    </div>
                    <div><input type="submit" value="搜索" class="btn_search" /></div>
                    <input type="button" value="增加" class="btn_add" onclick="location.href='${pageContext.request.contextPath }/admin/account.do?click=1';" />
                </div>  
                <!--删除等的操作提示-->
                <div id="operate_result_info" class="operate_success">
                    <img src="../images/close.png" onclick="this.parentNode.style.display='none';" />
                    删除成功，且已删除其下属的业务账号！
                </div>  
                <!--数据区域：用表格展示数据-->     
                <div id="data">            
                    <table id="datalist">
                    <tr>
                        <th>账号ID</th>
                        <th>姓名</th>
                        <th class="width150">身份证</th>
                        <th>登录名</th>
                        <th>状态</th>
                        <th class="width100">创建日期</th>
                        <th class="width150">上次登录时间</th>                                                        
                        <th class="width200"></th>
                    </tr>
                 <c:forEach items="${account}" var="account">
                    <tr>
                        <td>${account.accountID}</td>  
                        <td>${account.userName}</td>
                        <td>${account.idNumber}</td>
                        <td>${account.userAccount}</td>
                        <td>${account.state}</td>
                        <td>${account.createTime}</td>
                        <td>${account.lastLoginTime}</td>                            
                        <td class="td_modi">
                           <c:if test="${account.state=='开通'}" >
	                            <input type="button" value="暂停" class="btn_pause" onclick="setState(this);"/>
								<input type="button" value="修改" class="btn_modify" onclick="location.href='${pageContext.request.contextPath }/admin/account.do?click=3&accountID=${account.accountID}&userName=${account.userName}&idNumber=${account.idNumber}&userAccount=${account.userAccount}&userPhone=${account.userPhone}&reIdNumber=${account.reIdNumber}&reBirthday=${account.reBirthday}&reEmail=${account.reEmail}&reWork=${account.reWork}&reSex=${account.reSex}&reAddress=${account.reAddress}&reCode=${account.reCode}&reQQ=${account.reQQ}';" disabled="disabled"/>
	                            <input type="button" value="删除" class="btn_delete" onclick="deleteAccount(this);" disabled="disabled"/>
                        	</c:if>
                        	<c:if test="${account.state=='暂停'}">
	                            <input type="button" value="开通" class="btn_pause" onclick="setState(this);"/>
								<input type="button" value="修改" class="btn_modify" onclick="location.href='${pageContext.request.contextPath }/admin/account.do?click=3&accountID=${account.accountID}&userName=${account.userName}&idNumber=${account.idNumber}&userAccount=${account.userAccount}&userPhone=${account.userPhone}&reIdNumber=${account.reIdNumber}&reBirthday=${account.reBirthday}&reEmail=${account.reEmail}&reWork=${account.reWork}&reSex=${account.reSex}&reAddress=${account.reAddress}&reCode=${account.reCode}&reQQ=${account.reQQ}';" />
	                            <input type="button" value="删除" class="btn_delete" onclick="deleteAccount(this);" />
                        	</c:if>
                        </td>
                    </tr>
                  </c:forEach>                       
                </table>
               <p>业务说明：<br />
                1、创建则开通，记载创建时间；<br />
                2、暂停后，记载暂停时间；<br />
                3、重新开通后，删除暂停时间；<br />
                4、删除后，记载删除时间，标示为删除，不能再开通、修改、删除；<br />
                5、暂停账务账号，同时暂停下属的所有业务账号；<br />                
                6、暂停后重新开通账务账号，并不同时开启下属的所有业务账号，需要在业务账号管理中单独开启；<br />
                7、删除账务账号，同时删除下属的所有业务账号。</p> 
                </div>                   
                <!--分页-->
                <div id="pages">
                  <c:choose>
	                <c:when test="${page.curPage>1}">
	        	    	<a href="../admin/showView.do?click=5&curPage=${page.curPage-1}">上一页</a>
	        	    </c:when >
	        	    <c:otherwise><p style="display: inline-block;padding:8px 12px;">上一页</p></c:otherwise>
	        	</c:choose>
	        	
	        	<c:forEach items="${curPageNum}" var="curPage">
	        		<a href="../admin/showView.do?click=5&curPage=${curPage+1}">${curPage+1}</a>
	        	</c:forEach> 
	        
	        	<c:choose>   	    
	                <c:when test="${page.curPage<page.totalPage}">
	                	<a href="../admin/showView.do?click=5&curPage=${page.curPage+1}">下一页</a>
	                </c:when >
	        	    <c:otherwise><p style="display: inline-block;padding:8px 12px;">下一页</p></c:otherwise>
	        	</c:choose>
                </div>                    
            </form>
        </div>
        <!--主要区域结束-->
        <div id="footer">
            
        </div>
    </body>
</html>
